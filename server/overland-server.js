"use strict";

process.title = 'overland-server';

var WebSocketServer = require('websocket').server;
const Promise = require('bluebird');
const http = Promise.promisifyAll(require('http'));
const url = require('url');
const formidable = require('formidable');
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');
const redis = Promise.promisifyAll(require('redis'));
const RedisRun = require('./redis-run');
const MongoClient = require('mongodb').MongoClient;
const nconf = require('nconf');
const RequestPromise = require('request-promise');
const shortid = require('shortid');
const Discord = require('discord.js');
const DiscordBot = require('./discord-bot');
const _activityTimeout = 12000;

/////
const _broadcastPermissions = [
  'delta',
  'ping',
  'update-sheets'
];

//
nconf.argv().env();
nconf.defaults({
  port: 5000,
  redis_port: 6379,
  log_debug: false
});

const _logDebug = nconf.get('log_debug');

///////
function formidablePromise (req, opts) {
  return new Promise(function (resolve, reject) {
    var form = new formidable.IncomingForm(opts)
    form.parse(req, function (err, fields, files) {
      if (err) return reject(err)
      resolve({ fields: fields, files: files })
    })
  })
}

function discordGetUserId(name, discriminator) {
  return `user:${name}#${discriminator}`;
}

/////////////////////////
// Note:  It might be better to break this out as a separate service
async function setupDiscord() {
  var discordToken = nconf.get('discord_bot_token');

  if(discordToken) {
    discordClient = await DiscordBot.connect(redis, discordToken)
  }
}

async function setupRedis() {
  try {
    redisConn = redis.createClient(nconf.get("redis_port"), nconf.get("redis_host"));
    await RedisRun.load(redisConn, 'get-stats');
  }
  catch(e) {
    console.log(e.message);
  }
}

async function setupMongo() {
  try {
    console.log("Connecting to mongodb");

    var client = await MongoClient.connect('mongodb://mongo:27017', { poolSize: 10 });
    mongoConn = client.db('overland');

    console.log("Connected successfully to mongo server");
  }
  catch(e) {
    console.log(e.message);
  }
}

/////////////////////////
var redisConn = null;
setupRedis();

/////////////////////////
var mongoConn = null;
setupMongo();

/////////////////////////
var discordClient = null;
setupDiscord();

var server = http.createServer(async function(request, response) {
  var requestTime = new Date();
  var requestUrl = url.parse(request.url, true);
  
  console.log(`${requestTime} Received request for ${request.url}`);

  // Special URL for redirecting to bot adding page
  if(requestUrl.pathname === '/addbot') {
    var appId = nconf.get('discord_client_id');

    response.writeHead(302, {
      'Location': `https://discordapp.com/oauth2/authorize?client_id=${appId}&scope=bot&permissions=0`
    });

    response.end();
    
  }
  else if(requestUrl.pathname === '/upload_file') {
    var uploadKey;
    var party;
    var uploadDir = nconf.get('upload_dir');
    var replace;
    var replacePath;

    try {
      if(!requestUrl.query.a) {
        throw new Error('Bad upload ID');
      }
      
      uploadKey = `upload:${requestUrl.query.a}`;
      
      var data = await authenticationPromise(JSON.parse(request.headers.authorization).access_token);
      var requestData = await redisConn.hgetallAsync(uploadKey);

      if(!requestData) {
        throw new Error('Unknown request');
      }

      var requestingParty = requestData.party;

      if(requestData.replace) {
        // We have to remove the client URL specific portion of the stored file location.
        replace = requestData.replace.replace('uploads/', '');
        replacePath = `${uploadDir}/${replace}`;
      }

      // Verify that file exists
      if(replace && !fs.existsSync(replacePath)) {
        throw new Error('Unknown file: ' + replacePath);
      }

      console.log(`Party: ${requestingParty}`);
      party = requestingParty;

      // Process file upload and delete key
      var uploads = await formidablePromise(request, {
        uploadDir: uploadDir
      });

      redisConn.del(uploadKey);
      response.writeHead(200);
      response.end("<root/>");

      //
      var fileInfo = uploads.files.file;
      var targetPath = replace || ('uploads/' + path.basename(fileInfo.path));

      //
      if(replace) {
        fs.unlinkSync(replacePath);
        fs.renameSync(fileInfo.path, `${uploadDir}/${targetPath}`)
        return;
      }

      const collection = mongoConn.collection('parties');
      var fileData = {}
      fileData[`files.${targetPath}`] = fileInfo.name;

      collection.updateOne({ key: party }, { $set: fileData });

    }
    catch(e) {
      console.log(e.message);
      response.writeHead(401);
      response.end("<h2>bummer</h2>");
    }
  }
});

server.listen(nconf.get('port'), function(){});

console.log("Listening on port " + nconf.get('port'));

//////////////////////// Service
var ws = new WebSocketServer({ httpServer: server });
var _serviceHandlers = {};

function registerHandler(type, handler) {
  _serviceHandlers[type] = handler;
}

function sendToClient(conn, data) {
  var output = JSON.stringify(data);

  if(_logDebug) {
    console.log("[OUT] " + output);
  }
  
  conn.sendUTF(output);
}

function dispatchUTF(conn, msg) {
  if(_logDebug) {
    console.log(msg);
  }

  // TODO:  Enforce authentication and limit which calls can be made to the appropriate authentication type (eg. full user auth vs. apikey)
  
  // TODO:  Detect malformed json
  var data = JSON.parse(msg);
  var handler = _serviceHandlers[data.type];
  
  if(handler) {
    handler(conn, data);
  }
  else {
    // TODO:  Log and send error to client
  }
}

ws.on('request', function(request) {
  var conn = {};
  conn.ws = request.accept(null, request.origin);
  conn.mongo = mongoConn;

  console.log("Client connected");

  // Make redis connection
  // TODO:  This seems like a duplicate sort of connection to that created in setupRedis()
  conn.redis = redis.createClient(nconf.get("redis_port"), nconf.get("redis_host"));

  // Create sub connection for receiving events regarding this party.
  conn.sub = conn.redis.duplicate();
  conn.sub.on("message", function(channel, message) {
    if(_logDebug) {
      console.log(`\tSUB: ${channel} \t ${message}`);
    }

    // This is the egress of the broadcast system
    sendToClient(conn.ws, {type: 'broadcast',
                           payload: JSON.parse(message)});
  });

  // TODO:  Error handling/reporting
  
  // API
  conn.ws.on('message', function(msg) {
    if(msg.type === 'utf8') {
      dispatchUTF(conn, msg.utf8Data);
    }
  });

  conn.ws.on('close', async function() {
    if(conn.redis) {
      conn.redis.quit();
    }

    if(conn.sub) {
      await conn.sub.unsubscribeAsync();
      conn.sub.quit();
    }
  });
  
});

//////////////////////// Service handlers
function checkUserBroadcastPermissions(conn, broadcast) {
  var adminRequired = broadcast.type in _broadcastPermissions;
  return !adminRequired || conn.admin;
}

const _validDeltaKeys = {
  data: true,
  fragments: true,
  poi: true,
  poi_pending: true,
  annotations: true
}

function deltaAddIfPresent(update, data, k) {
  var v = data[k];

  if(v){
    update.$set[k] = v;
  }
}

async function applyDelta(mongo, delta) {
  var $set = {}
  var $unset = {}

  try {
    for(var groupId in delta.a) {
      if(!_validDeltaKeys[groupId]) {
        continue;
      }

      var group = delta.a[groupId];

      // TODO: Verify that this is handled/sent
      if(groupId === 'data') {
        deltaAddIfPresent($set, group, 'name');
        deltaAddIfPresent($set, group, 'pixelsPerMile');
        deltaAddIfPresent($set, group, 'grid');
        deltaAddIfPresent($set, group, 'scale');
        deltaAddIfPresent($set, group, 'panX');
        deltaAddIfPresent($set, group, 'panY');
      }
      else {
        for(var k in group) {
          $set[`${groupId}.${k}`] = group[k];
        }
      }
    }

    //
    for(var groupId in delta.d) {
      if(!_validDeltaKeys[groupId]) {
        continue;
      }

      var group = delta.d[groupId];
      
      for(var i = 0; i < group.length; ++i) {
        $unset[`${groupId}.${group[i]}`] = "";
      }
    }

    var update = {}
    if(Object.keys($set).length > 0) {
      update.$set = $set;
    }

    if(Object.keys($unset).length > 0) {
      update.$unset = $unset;
    }
    
    const collection = mongo.collection('maps');
    await collection.updateOne({ key: delta.map }, update);
  }
  catch(e) {
    console.log(e);
  }
}

function broadcastMessage(conn, chan, data) {
  // TODO:  Should probably check the channel as well.
  if(checkUserBroadcastPermissions(conn, data)) {
    conn.redis.publish(chan, JSON.stringify(data));
    return true;
  }
  return false;
}

registerHandler('delta', (conn, data) => {
  if(conn.map && conn.admin && broadcastMessage(conn, `${conn.map}:delta`, data)) {
    applyDelta(conn.mongo, data.payload);
  }
});

function sendReauthorizationRequest(conn) {
  var authUrl = nconf.get('discord_oauth_url');
  
  var response = {
    type: 'auth-response',
    status: 403,
    payload: {
      redirect: authUrl,
      reason: 'no-code'
    }
  };

  sendToClient(conn.ws, response);
}

function authenticationPromise(token) {
  var endpoint = nconf.get('discord_api_endpoint');
  
  var userRequestOptions = {
    uri: `${endpoint}/users/@me`,
    headers: {
      'Content-Type': 'application/json',
      'User-Agent': 'Overland (https://overland.5pmcasual.com, 0.1)',
      Authorization: `Bearer ${token}`
    },
    json: true
  }

  return RequestPromise(userRequestOptions);
}

registerHandler('auth-apikey', async function(conn, data) {
  // TODO:  Get api auth info from Redis using data.payload.key
  // .Get the party from that and return it
  var providedKey = data.payload.key;
  var keyParty = await conn.redis.getAsync(`apikey:${data.payload.key}`);

  // Maybe log auth for security reasons

  var response = {
    type: 'auth-apikey-response',
    payload: {}
  };

  if(keyParty) {
    conn.apiauth = providedKey;

    response.status = 200;
    response.payload.party = keyParty;
  }
  else {
    response.status = 403;
  }
  
  sendToClient(conn.ws, response);
});

registerHandler('auth', async function(conn, data) {
  // Return auth token and list of parties

  // TODO:  Return error if no auth token present
  // .Validate auth token
  // .User will be set by return data if valid

  try {
    var userKey;

    if(!data.payload.code && !data.payload.token) {
      sendReauthorizationRequest(conn);
      return;
    }

    var authToken = data.payload.token;
    
    //
    if(!authToken) {
      var requestOptions = {
        method: 'POST',
        uri: nconf.get('discord_token_url'),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: {
          client_id: nconf.get('discord_client_id'),
          client_secret: nconf.get('discord_client_secret'),
          grant_type: 'authorization_code',
          code: data.payload.code,
          scope: 'identity',
          redirect_uri: nconf.get('url')
        },
        json: true
      }

      authToken = await RequestPromise(requestOptions);
    }

    conn.auth = authToken;
    var authData = await authenticationPromise(conn.auth.access_token);

    var user = discordGetUserId(authData.username, authData.discriminator);
    conn.user = user;
    userKey = user;

    const usersCollection = conn.mongo.collection('users');
    var userData = await usersCollection.findOne({ key: userKey });
    
    var payload = {
      id: userKey,
      parties: [],
      permissions: {},
    }
    
    payload.token = conn.auth;
    payload.discord_app_id = nconf.get('discord_client_id');

    // Update the user's account avatar URL
    var avatarUrl;

    if(authData.avatar) {
      avatarUrl = `https://cdn.discordapp.com/avatars/${authData.id}/${authData.avatar}`;
    }

    if(!userData) {
      // Create new user
      console.log("Creating new user: " + userKey);
      await usersCollection.insertOne({
        key: userKey,
        avatar: avatarUrl,
      });
    }
    else {
      payload.last_party = userData.last_party;
    }

    const partiesCollection = conn.mongo.collection('parties');
    const parties = await partiesCollection.find({ users: userKey },
                                                 { $projection: { key: 1, name: 1 } });

    await parties.forEach((party) => {
      payload.parties.push({ id: party.key,
                             name: party.name });
    });
    
    var response = {
      type: 'auth-response',
      status: 200,
      payload: payload
    };
    
    sendToClient(conn.ws, response);
    
  }
  catch(e) {
    console.log(e.message);

    // TEMP:  Do an auth redirect regardless of the error
    sendReauthorizationRequest(conn);
  }

  // TODO:  Handle key authorization failure
  // .Should return a redirect in that case.
});

registerHandler('createparty', async function(conn, data) {
  // TODO:  Limit number of user created parties

  var partyName = data.payload.name;
  var creator = conn.user;
  var partyId = 'party:' + shortid.generate();

  var response = {
    type: 'createparty-response',
    payload: {},
    
  }
  
  const userCollection = conn.mongo.collection('users');
  const userData = await userCollection.findOne({ key: creator });

  try {
    if(nconf.get('whitelist_only') && userData.permissions && userData.permissions.party_creation) {
      throw new Error("PartyCreation not allowed for user");
    }

    var party = {
      key: partyId,
      name: partyName,
      owner: creator,
      users: [ creator ],
      permissions: {},
    }

    // Initially, the creator is an admin
    party.permissions[creator] = 1;
    
    const partiesCollection = conn.mongo.collection('parties');
    await partiesCollection.insertOne(party);

    response.payload.id = partyId;
    response.status = 200;
    sendToClient(conn.ws, response);

  }
  catch(e) {
    console.log(e.message);
    response.status = 401;
    sendToClient(conn.ws, response);
  };
  
});

function userKeyToFriendlyName(key) {
  var name = /user:(.*?)#/.exec(key)[1].replace(/_/g, ' ');
  name = name.replace(/\b[a-z]/g, function() { return arguments[0].toUpperCase(); });
  name = name.replace(/\b[A-Z]\b/g, function() { return arguments[0] + '.'; });
  return name;
}

async function getPartyMaps(mongoConn, party) {
  const collection = mongoConn.collection('maps');
  var data = [];

  // Only return map key and name
  const maps = collection.find(
    { owner: party },
    { projection: {
      _id: 0,
      key: 1,
      name: 1,
    }});

  await maps.forEach((thisMap) => {
    data.push({ id: thisMap.key,
                name: thisMap.name });
  });

  return data;
}

registerHandler('loadparty', async function(conn, data) {
  var party = data.payload.party;
  var payload = {};
  var response = {
    type: 'loadparty-response'
  };

  try {
    var time = (new Date).getTime();
    var minActive = time - _activityTimeout;

    const partiesCollection = conn.mongo.collection('parties');
    payload = await partiesCollection.findOne({ key: party },
                                              { projection: { _id: 0 } });

    if(!conn.apiauth) {
      var req = conn.redis.multi();
      req.zadd(party + ':active', time, conn.user);
      req.zrangebyscore(party + ':active', minActive, time);

      var results = await req.execAsync();

      payload.active_users = results[1];
    }

    //
    payload.avatars = payload.avatars || {};
    payload.aliases = payload.aliases || {};
    payload.sheets = payload.sheets || {};

    // Pull avatars for each user if they don't have an override set for this party
    const usersCollection = conn.mongo.collection('users');
    // NOTE: Could get an exact listing of users that need to be queried for, but that seems like a fairly minor optimization here.
    const users = usersCollection.find({ key: { $in: payload.users } },
                                       { projection: { key: 1, alias: 1, avatar: 1 } });

    await users.forEach((user) => {
      if(!payload.avatars[user.key]) {
        payload.avatars[user.key] = user.avatar || 'static/default-avatar.png';
      }

      if(!payload.aliases[user.key]) {
        payload.aliases[user.key] = userKeyToFriendlyName(user.key);
      }
    });

    //
    payload.maps = await getPartyMaps(conn.mongo, party);

    // TEMP:  This should be done elsewhere
    payload.chat = await DiscordBot.pullChannelHistory(discordClient.conn, payload.discord) || [];
    
    payload.id = party;

    //
    var membership = payload.users.includes(conn.user);

    if(conn.apiauth) {
      // TODO:  Revalidate the API key
      membership = 1;
    }
    
    if(membership) {
      // NOTE:  Should the owner always be an admin?
      // .They should always be able to become one anyway.
      // NOTE:  apiauth == admin currently
      var permissions = payload.permissions && payload.permissions[conn.user]
      var admin = permissions == 1 || conn.apiauth;
      conn.admin = admin;
      payload.admin = admin;
      
      response.status = 200;
      response.payload = payload;

      //
      conn.sub.unsubscribe();
      conn.sub.subscribe(party);
      
      //
      conn.party = party;

      if(!conn.apiauth) {
        // Record this loading for this user
        usersCollection.updateOne({ key: conn.user },
                                  { $set: { last_party: party }});

        // Announce this load to the party
        var partyAnnounce = {type: 'party-user-announce',
                             status: 200,
                             payload: {
                               user: conn.user,
                               avatar: payload.avatars[conn.user],
                               alias: userKeyToFriendlyName(conn.user)
                             }
                            }

        conn.redis.publish(party, JSON.stringify(partyAnnounce));
      }

    }
    else {
      response.status = 403;
    }
    
    sendToClient(conn.ws, response);
  }
  catch(e) {
    // TODO:
    console.log(e.message);
  }
});

registerHandler('bindparty', async function(conn, data) {
  var invite = `discord-bind:${data.payload.invite}`;
  var party = data.payload.party;
  var response = {
    type: 'bindparty-response',
    payload: {}    
  }

  try {
    // TODO:  Verify that this user is an admin of the party.

    var bindingData = await conn.redis.hgetallAsync(invite);

    if(!bindingData) {
      throw("Unknown binding request");
    }
    
    var requestingUser = `user:${bindingData.user}`;
    
    if(requestingUser != conn.user) {
      throw('Incorrect user');
    }

    // TODO:  Check that this binding hasn't already been done.

    const collection = conn.mongo.collection('parties');
    collection.updateOne({ key: party },
                         { $set: { discord: bindingData.channel } });

    // TODO:  discord-party mapping still lives in redis
    
    var batch = conn.redis.multi();
    batch.set(`discord-party:${bindingData.channel}`, party);
    batch.del(invite);
    batch.exec();

    //
    response.status = 200;
    response.payload.party = party;
    sendToClient(conn.ws, response);
  }
  catch(e) {
    console.log(e.message);
  }
});

registerHandler('accept-invite', async function(conn, data) {
  var invite = `invite:${data.payload.invite}`;

  var response = {
    type: 'accept-invite-response',
    payload: {
    }
  }

  try {
    var inviteData = await conn.redis.hgetallAsync(invite);

    if(!inviteData) {
      throw('Unknown invitation');
    }
    
    response.payload.party = inviteData.party;
      
    var destroy = false;
    
    if(inviteData.user) {
      if(inviteData.user != conn.user) {
        throw('Incorrect user accepted invitation');
      }

      destroy = true;
    }

    const collection = conn.mongo.collection('parties');
    collection.updateOne({ key: inviteData.party },
                         { $addToSet: { users: conn.user } });

    if(destroy) {
      conn.redis.del(invite);
    }

    response.status = 200;
    sendToClient(conn.ws, response);
  }
  catch(e) {
    console.log(e);
  }

  // TODO:  Error handling
});

registerHandler('createmap', (conn, data) => {
});

function validCloneType(type) {
  if(type == 'map' || type == 'template') {
    return true;
  }

  return false;
}

registerHandler('clonemap', async function(conn, data) {
  // map -> template, template -> map, map -> map
  try {
    var sourceType = data.payload.source_type;
    var destType = data.payload.destination_type;
    var id = data.payload.source_id;
    var initialMapConfig = data.payload.config;

    if(!validCloneType(sourceType) || !validCloneType(destType)) {
      throw('Invalid sourc or destination type');
    }

    var source = `${sourceType}:${id}`;
    var destId = shortid.generate();
    var dest = `${destType}:${destId}`;

    // TODO:  Verify unique key

    //
    console.log("Cloning %s -> %s", source, dest);

    const templatesCollection = conn.mongo.collection('templates');
    var template = await templatesCollection.findOne({ key: source });

    if(!template) {
      throw(`Non-existant source map for cloning: ${source}`);
    }
    
    if(sourceType === 'map') {
      // TODO:  Allow for cloning across parties if you are an admin in both. 
      if(template.owner != conn.party || !template.public ) {
        throw('Invalid access to source map on clone');
      }
    }

    // Set the owner
    if(destType === 'map') {
      template.owner = conn.party;
    }

    // Apply any provided data
    for(var k in initialMapConfig) {
      switch(k) {
      case 'name':
      case 'pixelsPerMile':
        template[k] = initialMapConfig[k];
        break;
      }
    }

    delete template._id;
    template.key = dest;
    
    // 
    const mapsCollection = conn.mongo.collection('maps');
    await mapsCollection.insert(template);
    
    var response = {
      type: 'clonemap-response',
      status: 200,
      payload: {
        created: dest,
        maps: await getPartyMaps(conn.mongo, conn.party)
      }
    }
    sendToClient(conn.ws, response);
  }
  catch(e) {
    console.log(e);
  }
});

registerHandler('loadmap', async function(conn, data) {
  try {
    // Pull all of the necessary data
    var map = data.payload.map;
    var payload;
    var response = {
      type: 'loadmap-response',
    };

    if(typeof(map) != 'string') {
      throw new Error('Invalid map ID');
    }
    
    console.log("LOADING MAP: " + map);

    // TODO:  Filter data output by permissions

    const mapsCollection = conn.mongo.collection('maps');
    payload = await mapsCollection.findOne({ key: map },
                                           { $projection: { _id: 0 } });

    payload.id = payload.key;
    payload.grid = payload.grid || [];
    payload.fragments = payload.fragments || {};
    payload.icons = payload.icons || {};
    payload.poi = payload.poi || {};
    payload.poi_pending = payload.poiPending || {};
    payload.annotations = payload.annotations || {};

    //
    if(payload.owner == conn.party) {
      response.status = 200;
      response.payload = payload;

      conn.map = map;
      
      // HACK:  Had trouble with unsubscribing from specific channels
      // .Instead nuking everything and re-subscribing to anything that's necessary
      conn.sub.unsubscribe();
      conn.sub.subscribe(conn.party);
      conn.sub.subscribe(map);
      conn.sub.subscribe(`${map}:delta`);

      if(conn.admin) {
        conn.sub.subscribe(`${map}:delta:hidden`);
      }
    }
    else {
      response.status = 403;
    }
    
    sendToClient(conn.ws, response);

  }
  catch(e) {
    console.log(`loadmap exception: ${e.message}`);
    response.status = 500;
    sendToClient(conn.ws, response);
  }
});

registerHandler('party-heartbeat', async function(conn, data) {
  try {
    var response = {
      type: 'party-heartbeat-response',
      payload: {
      }
    }
    
    if(!conn.party || !conn.user) {
      response.status = 410;
      sendToClient(conn.ws, response);
      return true;
    }

    var activePartyKey = `${conn.party}:active`;
    var time = (new Date).getTime();
    var min = time - _activityTimeout;

    var write = conn.redis.multi();
    write.zadd(activePartyKey, time, conn.user);
    write.zrangebyscore(activePartyKey, min, time);
    write.expire(activePartyKey, 300);

    var results = await write.execAsync();

    response.payload.active_users = results[1];
    
    response.status = 200;
    sendToClient(conn.ws, response);
  }
  catch(e) {
    console.log(e.message);
  }
  
  return true;
});

registerHandler('party-switchmap', async function(conn, data) {
  var requestedMap = data.payload.map;
  
  var response = {
    type: 'party-switchmap-response'
  }

  // TODO:  Check that party is loaded and that the necessary data has been provided
  
  if(conn.admin) {
    response.status = 200;
    
    // Set party current_map
    // TODO:  Verify map exists

    const collection = conn.mongo.collection('parties');
    collection.updateOne({ key: conn.party },
                         { $set: { current_map: requestedMap }});

    var partyRequest = {type: 'party-switchmap',
                        status: 200,
                        payload: {
                          map: requestedMap
                        }
                       }
    
    conn.redis.publish(conn.party, JSON.stringify(partyRequest));
  }
  else {
    response.status = 403;
  }

  sendToClient(conn.ws, response);
});

registerHandler('party-files', async function(conn, data) {
  var response = {
    type: 'party-files-response',
    payload: {}
  }

  try {
    if(!conn.party) {
      throw new Error("Party not loaded");
    }
    
    const collection = conn.mongo.collection('parties');
    var partyData = await collection.findOne(
      { key: conn.party },
      { $projection: { files: 1 } });
    
    response.status = 200;
    response.payload.files = partyData.files;
    sendToClient(conn.ws, response);
  }
  catch(e) {
    console.log(e.message);

    response.status = 403;
    sendToClient(conn.ws, response);
  }
});

registerHandler('upload-party-sheets', async function(conn, data) {
  var response = {
    type: 'upload-party-sheets-response',
    payload: {}
  }

  try {
    // TEMP:
    throw("Sheets support Unimplemented");

    // if(!conn.party) {
    //   throw new Error("Party not loaded");
    // }

    // var sheets = [];
    // for(var k in data.payload.sheets) {
    //   sheets.push(k);
    //   sheets.push(JSON.stringify(data.payload.sheets[k]));
    // }

    // if(!sheets) {
    //   throw new Error("No sheets specified");
    // }
    
    // await conn.redis.hmsetAsync(`${conn.party}:sheets`, sheets);
    
    // response.status = 200;
    // sendToClient(conn.ws, response);

    // // TODO:  Also broadcast the changes to the clients
    // broadcastMessage(conn, conn.party,
    //                  { type: 'update-sheets',
    //                    status: 200,
    //                    payload: data.payload.sheets });
    
  }
  catch(e) {
    console.log(e.message);
    
    response.status = 403;
    sendToClient(conn.ws, response);
  }
});

registerHandler('request-upload', async function(conn, data) {
  var id = shortid.generate();
  var key = `upload:${id}`;

  var response = {
    type: 'request-upload-response',
    payload: {}
  }

  if(!conn.party || !conn.admin) {
    throw new Error("Not admin of party");
  }
  
  try {
    var replacing;
    
    if(data.payload.replace) {
      replacing = data.payload.replace;

      // TODO: Determine if this is still useful
      // var files = await conn.redis.hgetallAsync(`${conn.party}:files`);
      // if(!files || !files[replacing]) {
      //   throw new Error("No file found in party");
      // }
    }
    
    var write = conn.redis.multi();
    write.hset(key, 'party', conn.party);

    if(replacing) {
      write.hset(key, 'replace', replacing);
    }
    
    write.expire(key, 120);

    await write.execAsync();

    response.status = 200;
    response.payload.upload_id = id;
    sendToClient(conn.ws, response);
  }
  catch(e) {
    console.log(e.message);
    
    response.status = 401;
    sendToClient(conn.ws, response);
  }
});

registerHandler('ping', (conn, data) => {
  if(conn.map && broadcastMessage(conn, `${conn.map}`, data)) {
    // TODO:  Verify map permissions
  }
});

registerHandler('party-stats', async function(conn, data) {
  var response = {
    type: 'party-stats-response',
    payload: {}
  }

  try {
    if(!conn.party) {
      throw new Error("Party not loaded");
    }

    var dates = await DiscordBot.analyticsGetDates(conn.redis, conn.party);

    for(var i = 0; i < dates.length; ++i) {
      var entry = dates[i];
      response.payload[entry] = await DiscordBot.analyticsGetStats(conn.redis, conn.party, entry);
    }

    response.status = 200;
    sendToClient(conn.ws, response);
  }
  catch(e) {
    console.log(e.message);
    response.status = 500;
    sendToClient(conn.ws, response);
  }
});

