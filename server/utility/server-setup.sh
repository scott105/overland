#!/bin/bash -e

# Create the overland user if necessary
id -u overland &>/dev/null || useradd --system --create-home --home-dir /opt/overland/home overland

# Systemd
mkdir -p /usr/lib/systemd/system
cp `dirname $0`/overland.service /usr/lib/systemd/system/
systemctl enable overland.service
systemctl daemon-reload
