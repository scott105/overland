const Discord = require('discord.js');
const RedisRun = require('./redis-run');
const shortid = require('shortid');
const nconf = require('nconf');
const MongoClient = require('mongodb').MongoClient;

var _discordHandlers = {};
var _documentation = {}

async function connect(redis, token) {
  var discordClient = {}
  discordClient.conn = new Discord.Client();
  discordClient.redis = redis.createClient(nconf.get("redis_port"), nconf.get("redis_host"));

  var mongoClient = await MongoClient.connect('mongodb://mongo:27017', { poolSize: 10 });
  discordClient.mongo = mongoClient.db('overland');

  discordClient.conn.on('ready', () => {
    console.log(`Discord bridge connect: ${discordClient.conn.user.tag}`);
  });

  discordClient.conn.on('disconnect', () => {
    disconnect.redis.quit();
  });

  discordClient.conn.on('message', (msg) => {
    // Get the party id for this channel, if any
    discordClient.redis.getAsync(`discord-party:${msg.channel.id}`).then((partyKey) => {
      console.log(`Discord: ${msg.channel.id}> ${msg.author.username}: ${msg.content}`);

      dispatchDiscord(discordClient, msg, partyKey);
    });
  });

  discordClient.conn.login(token);

  return discordClient
}

function discordSetChatAnalytics(write, party, msg) {
  var timestamp = new Date(msg.createdAt);
  var period = 2;
  var minutes = timestamp.getHours() * 60 + timestamp.getMinutes();
  var offset = Math.floor(minutes / period);
  var keyDate = timestamp.toISOString().substring(0, 10);

  var partyKey = `${party}:stats:ca:${keyDate}`;
  write.setbit(partyKey, offset, 1);

  if(!msg.webhookID && !msg.author.bot) {
    var userKey = `user:${msg.author.tag}:stats:ca:${keyDate}`;
    write.setbit(userKey, offset, 1);
  }
}

async function pullChannelHistory(client, channelId) {
  try {
    var channel = client.channels.get(channelId);

    if(!channel) {
      return;
    }

    var options = { limit: 50 }
    var messages = await channel.fetchMessages(options);

    var history = [];

    for(var [flake, msg] of messages) {
      if(msg.content.charAt(0) != ',') {
        history.push({
          id: msg.id,
          user: msg.author.username,
          tag: msg.author.tag,
          msg: msg.content
        });
      }
    }

    return history;
  }
  catch(e) {
    console.log('CHAT', e.message);
  }
}

async function processDiscordHistory(conn, partyKey, channel) {
  var options = { limit: 100 }

  var count = 0;

  while(true) {
    var messages = await channel.fetchMessages(options);
    var first = true

    if(!messages || messages.size < 1) {
      break;
    }

    count += messages.size;
    // console.log(messages.size);

    var write = conn.multi();

    for(var [flake, msg] of messages) {
      discordSetChatAnalytics(write, partyKey, msg);

      if(first) {
        first = false;
        options.before = flake;
      }

      // console.log(msg.content);
    }

    write.exec();
  }

  return count;
}

function registerHandler(cmd, doc, handler) {
  _discordHandlers[cmd] = handler;
  _documentation[cmd] = doc
}

async function dispatchDiscord(client, msg, partyKey) {

  if(msg.content.charAt(0) == ',') {
    // Don't try to handle our own commands
    if(msg.author.id == client.conn.user.id) {
      return;
    }

    var input = msg.content.split(/\s+/);
    var cmd = input[0];
    var params = input.slice(1);

    var handler = _discordHandlers[cmd];

    if(handler) {
      return handler(client, msg, params, partyKey);
    }
    else {
      console.log(`Unkown discord command ${cmd}`);
      return false;
    }
  }

  // Send non-commands to the appropriate party
  var chat = {
    type: 'chat',
    status: 200,
    payload: {
      id: msg.id,
      user: msg.author.username,
      tag: msg.author.tag,
      msg: msg.content
    }
  }

  client.redis.publish(partyKey, JSON.stringify(chat));
}

registerHandler(',link', "Returns the Overland link for the party", (client, msg, params, partyKey) => {
  if(!partyKey) {
    msg.channel.send('*Somebody* better start this party.');
    return false;
  }

  var instanceUrl = nconf.get('url') + `?party=${partyKey}`;
  msg.channel.send(`Head on over to ${instanceUrl}`);
});

registerHandler(',icametoparty', "Register channel to party (Admin)", (client, msg, params, partyKey) => {
  // Generate an invite ID and write it to redis and as a link into redis
  // Following the link prompts to select a party that the user is an admin for
  // That sends a request to the service to complete the binding
  // The service verifies that that user is an admin of the party and then sets the binding

  // Only a channel admin is allowed to do this
  var permissions = msg.channel.memberPermissions(msg.author);
  if(!permissions.has('ADMINISTRATOR')) {
    msg.channel.send(`Insufficient permissions for partying`);
    return false;
  }

  if(partyKey) {
    msg.channel.send(`It would seem that you, or someone very much like you, already came to party on behalf of ${partyKey}`);
    return false;
  }

  // Write invite data to redis with expire time and drop link
  var inviteId = shortid.generate();
  var inviteKey = `discord-bind:${inviteId}`;

  var write = client.redis.multi();
  write.hmset(inviteKey, 'channel', msg.channel.id, 'user', msg.author.tag);
  write.expire(inviteKey, 300);
  write.exec();

  var inviteUrl = nconf.get('url') + `?bindchannel=${inviteId}`;
  msg.channel.send(`Psssst... the party is over here... ${inviteUrl}`);
})

registerHandler(',lameparty', "Unregister this channel from party (Admin)", (client, msg, params, partyKey) => {
  // Unbind party
  if(!partyKey) {
    msg.channel.send('Party?  What party?');
    return false;
  }

  var permissions = msg.channel.memberPermissions(msg.author);

  if(!permissions.has('ADMINISTRATOR')) {
    msg.channel.send(`Insufficient permissions for *stopping this partying*`);
    return false;
  }

  client.redis.del(`discord-party:${msg.channel.id}`);

  const collection = client.mongo.collection('parties');
  collection.updateOne({ key: partyKey },
                       { $unset: { discord: '' } });

  msg.channel.send(`Well fine then.  ${partyKey} is over.  Get out.`);
});

registerHandler(',invite', "Invite a user to this party (Admin)", async function(client, msg, params, partyKey) {
  var inviteTTL = 300;

  if(!partyKey) {
    msg.channel.send(`You arrive at a darkened house.  There is no party.  There never was.  The mind reels.  You sense your footing giving way to a tumultuous earth.  It's like the end of the new Twin Peaks where the house goes dark and stuff...`);
    return false;
  }

  // TODO:  If params are specified, verify that they're usernames and create bound links for each
  // .Those could be sent as DMs
  // .Otherwise, create an open link and drop that
  // .Maybe provide a link to invite everyone in the channel

  var creator = `user:${msg.author.tag}`;

  try {
    const collection = client.mongo.collection('parties');
    var partyData = await collection.findOne({ key: partyKey },
                                             { $projection: { permissions: 1 } });

    if(!partyData || !partyData.permissions || partyData.permissions[creator] != 1) {
      throw(`${creator} is not an admin of ${partyKey}`);
    }

    for(var [id, user] of msg.mentions.users) {
      if(user.bot) {
        continue;
      }

      var userTag = `${user.username}#${user.discriminator}`;

      // Write invite data to redis with expire time and drop link
      var inviteId = shortid.generate();
      var inviteKey = `invite:${inviteId}`;

      var write = client.redis.multi();
      write.hmset(inviteKey, 'party', partyKey, 'user', `user:${user.tag}`);
      write.expire(inviteKey, inviteTTL);
      write.exec();

      var inviteUrl = nconf.get('url') + `?invite=${inviteId}`;
      user.send(`This is where you can get in on this party ${inviteUrl}`);
    }

    if(msg.mentions.everyone) {
      var inviteId = shortid.generate();
      var inviteKey = `invite:${inviteId}`;

      var write = client.redis.multi();
      write.hmset(inviteKey, 'party', partyKey);
      write.expire(inviteKey, inviteTTL);
      write.exec();

      var inviteUrl = nconf.get('url') + `?invite=${inviteId}`;
      msg.channel.send(`This is where you can get in on this party ${inviteUrl}`);
    }
  }
  catch(e) {
    console.log(e.message);
  }
});

registerHandler(',aup', "Update the channel analytics (Admin)",  async function(client, msg, params, partyKey) {
  if(!partyKey) {
    msg.channel.send('Like, do you even party?  Gotta party...');
    return false;
  }

  var permissions = msg.channel.memberPermissions(msg.author);

  if(!permissions.has('ADMINISTRATOR')) {
    msg.channel.send(`Insufficient permissions for *publicly shaming your players*`);
    return false;
  }

  var count = await processDiscordHistory(client.redis, partyKey, msg.channel);
  msg.author.send(`Processed ${count} messages`);
});

async function analyticsGetStats(conn, partyKey, start, detailed) {
  var stats = await RedisRun.exec(conn, 'get-stats', 1, partyKey, start, detailed ? 1 : 0);
  stats = JSON.parse(stats);

  if(!detailed) {
    for(var user in stats) {
      stats[user] = stats[user] * 2;
    }
  }

  return stats;
}

registerHandler(',astats', "Analytics stats for channel (Admin)",   async function(client, msg, params, partyKey) {
  if(!partyKey) {
    msg.channel.send('Like, do you even party?  Gotta party...');
    return false;
  }

  var permissions = msg.channel.memberPermissions(msg.author);

  if(!permissions.has('ADMINISTRATOR')) {
    msg.channel.send(`Insufficient permissions for *publicly shaming your players*`);
    return false;
  }

  //
  try {
    var start = params[0] || (new Date).toISOString().substring(0, 10);
    var results = await analyticsGetStats(client.redis, partyKey, start);

    var out = `\`\`\`Stats for ${start}:\n`;
    for(var user in results) {
      out += `${user}:  ${results[user]}\n`;
    }
    out += "```";

    msg.channel.send(out);
  }
  catch(e) {
    msg.channel.send(`\`\`\`ERROR: ${e.message}\`\`\``);
  }
});

async function analyticsGetDates(conn, partyKey) {
  // Get all key entries for this party
  var keySpace = `${partyKey}:stats:ca:`;
  var cursor = 0;
  var results = [];

  while(true) {
    var data = await conn.scanAsync(cursor, "match", `${keySpace}*`);

    data[1].forEach(function(entry) {
      results.push(entry.substring(keySpace.length));
    });

    cursor = data[0];

    if(data[0] == 0) {
      break;
    }
  }

  results.sort();

  return results;
}

registerHandler(',adates', "Return analytics information (Admin)",  async function(client, msg, params, partyKey) {
  if(!partyKey) {
    msg.channel.send('Like, do you even party?  Gotta party...');
    return false;
  }

  var permissions = msg.channel.memberPermissions(msg.author);

  if(!permissions.has('ADMINISTRATOR')) {
    msg.channel.send(`Insufficient permissions for *publicly shaming your players*`);
    return false;
  }

  //
  try {
    var results = await analyticsGetDates(client.redis, partyKey);
    msg.channel.send(`\`\`\`Dates:\n${results.join('\n')}\`\`\``);
  }
  catch(e) {
    msg.channel.send(`\`\`\`ERROR: ${e.message}\`\`\``);
  }
});

registerHandler(',r', "Roll some dice", (client, msg, params, partyKey) => {
  var d20 = Math.floor(Math.random() * 20) + 1
  msg.channel.send(`||${d20}  =  (${d20})||   =   d20   ${msg.author}`)
});

registerHandler(',?', "This message", (client, msg, params, partyKey) => {
  var out = [];

  out.push("```")

  for(var cmd in _discordHandlers) {
    var doc = _documentation[cmd]
    out.push(`${cmd.padEnd(16, " ")}${doc || ''}`);
  }

  out.push("```")

  msg.channel.send(out.join('\n'));
});

////////////////////////////////////////////////////////////////
exports.connect = connect
exports.registerHandler = registerHandler
exports.analyticsGetDates = analyticsGetDates
exports.analyticsGetStats = analyticsGetStats
exports.pullChannelHistory = pullChannelHistory;
