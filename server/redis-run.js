const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');
const redis = Promise.promisifyAll(require('redis'));

var _redisScripts = {}

async function load(conn, name) {
  try {
    var filename = path.join('./redis_scripts', `${name}.lua`);
    var contents = fs.readFileSync(filename);
    var sha1 = await conn.scriptAsync("load", contents);

    console.log(`Loading redis script ${name}: ${sha1}`);
    _redisScripts[name] = sha1;
  }
  catch(e) {
    console.log(e.message);
  }
}

async function exec(conn, name, ...rest) {
  var sha = _redisScripts[name];
  return await conn.evalshaAsync(sha, ...rest)
}

exports.load = load;
exports.exec = exec;
