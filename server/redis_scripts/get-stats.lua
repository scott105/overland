local partyKey = KEYS[1]
local start = ARGV[1]
local detailed = ARGV[2] == 1

local results = {}

local function toMask(key)
   local mask = {}

   for i = 1, 24 * 30 do
      table.insert(mask, redis.call('getbit', key, i - 1))
   end

   return mask
end

local partyUsers = redis.call('hgetall', partyKey .. ':users')

if detailed then
   results.channel = toMask(partyKey .. ':stats:ca:' .. start)
else
   results.channel = redis.call('bitcount', partyKey .. ':stats:ca:' .. start)
end

for i = 1, #partyUsers, 2 do
   local user = partyUsers[i]
   local statsKey = user .. ':stats:ca:' .. start

   if detailed then
      results[user] = toMask(statsKey)
   else
      results[user] = redis.call('bitcount', statsKey)
   end
end

return cjson.encode(results)
