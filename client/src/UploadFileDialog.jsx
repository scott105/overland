import * as Overland from './overland.js';

import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function UploadFileDialog(props) {

  const [currentFiles, setCurrentFiles] = React.useState();

  const onFileSelected = (ev) => {
    setCurrentFiles(ev.target.files);
  };

  const onUpload = () => {
    // console.log(`Uploading ${currentFiles[0].name} of type ${currentFiles[0].type}`);
    var req = {type: 'request-upload', payload: {}};
    var fileData = currentFiles[0];

    const supportedFilesTypes = ['image/jpeg', 'image/png'];

    if(supportedFilesTypes.indexOf(fileData.type) < 0) {
      // TODO:  Flag specific selections as incorrect
      console.log("Incorrect file type for upload");
      return;
    }

    Overland.sendToService(global._serverConnection,
                  req,
                  (response) => {
                    var uploadId = response.payload.upload_id;

                    const reader = new FileReader();
                    // reader.onload = e => setPreview(e.target.result);
                    reader.readAsDataURL(fileData);

                    var data = new FormData();
                    data.append('file', fileData);

                    const xhr = new XMLHttpRequest();

                    xhr.upload.onprogress = (e) => {
                      const done = e.position || e.loaded;
                      const total = e.totalSize || e.total;
                      const perc = (Math.floor(done/total*1000)/10);

                      // TODO: Loading bar
                      console.log(`Progress: ${perc}`);
                    };

                    xhr.upload.onload = (e) => {
                      // TODO:  Snack bar and possibly not closing the dialog
                      props.appControl.closeActiveDialog();
                    };

                    xhr.open('POST', `/upload_file?a=${uploadId}`);
                    xhr.setRequestHeader('Authorization', JSON.stringify(props.userData.token));
                    xhr.send(data);
                  });
  };

  return (
    <Dialog open={props.open} onClose={props.appControl.closeActiveDialog} aria-labelledby="form-dialog-title" >
      <DialogTitle id="form-dialog-title">File Upload</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Upload a file and do stuff with it.
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="File"
          type="file"
          onChange={onFileSelected}
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.appControl.closeActiveDialog} color="primary">
          Cancel
        </Button>
        <Button onClick={onUpload} color="primary">
          Upload
        </Button>
      </DialogActions>
    </Dialog>
  );
}
