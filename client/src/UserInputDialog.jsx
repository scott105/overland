import React, { Component, useEffect, useState } from 'react';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function UserInputDialog(props) {
  const [field, setField] = useState(props.initialValue);

  // Note:  If field isn't used then true/false are called as results with the button
  var requiresField = true;
  var fieldType = 'text';
  var cancelText = 'Cancel';
  var submitText = 'Submit';

  const onFieldChange = (e) => {
    setField(e.target.value);
  };

  const onSubmit = () => {
    if(props.onCompleted) {
      props.onCompleted(requiresField ? field : true);
    }

    props.appControl.clearInputPrompt();
  };

  const onClose = () => {
    if(!requiresField && props.onCompleted) {
      props.onCompleted(false);
    }

    props.appControl.clearInputPrompt();
  };

  switch(props.type) {
  case 'yesno':
    requiresField = false;
    submitText = 'Confirm';
    break;

  case 'number':
    fieldType = 'number';
    break;
  }

  return (
    <Dialog
      open={true}
      onClose={onClose}
      aria-labelledby="form-dialog-title" >
      <DialogTitle id="form-dialog-title">{props.title}</DialogTitle>
      <form onSubmit={onSubmit}>
        {requiresField &&
         <DialogContent>
           <TextField
             autoFocus={true}
             margin="dense"
             id="dataField"
             label={props.field}
             type={fieldType}
             fullWidth={true}
             defaultValue={props.initialValue}
             onChange={onFieldChange}
           />
         </DialogContent>
        }
        <DialogActions>
          <Button onClick={onClose} color="primary">
            {cancelText}
          </Button>
          <Button onClick={onSubmit} color="primary">
            {submitText}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}
