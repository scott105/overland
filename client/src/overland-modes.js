//
function forEachFragObj(f) {
  var container = _konvaStage.findOne('#fragment-container');
  container.children.forEach(f);
}

registerModeHandler(
  Modes.FRAG,

  function () {
    console.log("Entering frag mode");

    // Enable the frag panel
    $('#properties-panel #fragment-properties').toggle(true);
    var fragPanel = $('#properties-panel #fragment-properties #map-fragment-panel');
    fragPanel.toggle(false);
    
    // Setup event handlers for fragment objects
    forEachFragObj(function(fragObj) {
      fragObj.on('click', function(obj) {
        forEachFragObj(function(other) {
          other.draggable(false);
          other.strokeEnabled(false);
        });
        
        fragObj.draggable(true);
        fragObj.strokeWidth(10);
        fragObj.stroke('yellow');
        fragObj.strokeEnabled(true);

        var fragName = fragObj.fragName;
        var data = _mapData.fragments[fragName];

        _currentFragment = fragName;
        updateMapFragmentPanel(fragName, data);
        fragPanel.toggle(true);

        fragObj.on('dragmove', function(ev) {
          var thisFrag = ev.currentTarget;
          data.x = thisFrag.x() / _mapData.data.pixelsPerMile;
          data.y = thisFrag.y() / _mapData.data.pixelsPerMile;
          updateMapFragmentPanel(fragName, data);
        });

        fragObj.on('dragend', function(ev) {
          var thisFrag = ev.currentTarget;
          var delta = {fragments: {}}
          delta.fragments[fragName] = data;
          
          sendMapDelta(delta);
        });

        _konvaStage.draw();
      });
    });
  },
  
  function () {
    console.log("Exiting frag mode");

    // Disable the frag panel
    $('#properties-panel #fragment-properties').toggle(false);
    
    forEachFragObj((obj) => {
      obj.draggable(false);
      obj.off('click dragmove dragend');
      obj.strokeEnabled(false);
    });

    _konvaStage.draw();
  });

//
registerModeHandler(
  Modes.POI,
  () => {
    console.log("Entering poi mode");
  },
  () => {
    console.log("Exiting poi mode");
  });

