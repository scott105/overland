import React from 'react';
import SimpleMenu from './SimpleMenu';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';

import * as Overland from './overland.js';

import { Component } from 'react';

export default class UserMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  render() {
    var userData = this.props.userData;

    if(!userData || !userData.parties) {
      return null;
    }

    var appControl = this.props.appControl;
    var parties = userData.parties;

    return (
      <div>
        <MenuList>
          <div>User</div>
          <div>
            {parties.length > 0 &&
            <SimpleMenu id='LoadPartyMenu' heading='Load Party'
                        onSelection={(partyId) =>
                          Overland.doPartyLoad(global._serverConnection, {party: partyId})}>

              {parties.map(item => <MenuItem key={item.id} id={item.id}>{item.name}</MenuItem>)}
            </SimpleMenu>}
          </div>
          <MenuItem key='createParty' onClick={() => {appControl.openDialog('create_party');}}>Create Party</MenuItem>
          <MenuItem key='LogOut' onClick={() => appControl.logout()}>Log Out</MenuItem>
        </MenuList>

      </div>
    );
  }
}
