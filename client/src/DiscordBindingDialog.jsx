import * as Overland from './overland.js';

import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';

export default function DiscordBindingDialog(props) {
  const [selectedParty, setSelectedParty] = React.useState();

  const onBind = () => {
    console.log(selectedParty);
    if(selectedParty) {
      var bindingId = global._queryParams.get('bindchannel');
      Overland.sendBindingRequest(bindingId, selectedParty);
      props.appControl.closeActiveDialog();
    }
  };

  return (
    <Dialog open={props.open} onClose={props.appControl.closeActiveDialog} aria-labelledby="form-dialog-title" >
      <DialogTitle id="form-dialog-title">Bind Discord Channel</DialogTitle>
      <DialogContent>
        <DialogContentText>
          So you came to party?
        </DialogContentText>
        <List>
          {props.userData.parties.map(party => (
            <ListItem button key={party.id}
                      onClick={() => setSelectedParty(party.id)}
                      selected={selectedParty === party.id}>
              <ListItemText primary={party.name} />
            </ListItem>
          ))}
        </List>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.appControl.closeActiveDialog} color="primary">
          Cancel
        </Button>
        <Button onClick={onBind} color="primary">
          Bind
        </Button>
      </DialogActions>
    </Dialog>
  );
}
