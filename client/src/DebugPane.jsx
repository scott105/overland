import React from 'react';
import { Component } from 'react'

function UserInfo(props) {
  if(props.userData.id) {
    return (
      <>
        <span>User: {props.userData.id}</span>
        <span>Last Party: {props.userData.last_party}</span>
      </>
    )
  }

  return null
}

function PartyInfo(props) {
  var data = props.partyData

  if(data.id) {
    return (
      <>
        <span>{data.name} ({data.id})</span>
        {/* {data.admin && <span>Admin</span>} */}
        {/* <span>{data.maps.length} maps, {Object.keys(data.users).length} users</span> */}
        {/* <span>Owner: {data.party.owner}</span> */}
      </>
    )
  }

  return null
}

function MapInfo(props) {
  var data = props.mapData

  if(data.id) {
    return (
      <>
        <span>Map: {data.name} ({data.id})</span>
      </>
    )
  }

  return null
}

export default class DebugPane extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    var props = this.props

    return (
      <div key='debugView' className='DebugPane'>
        {/* <UserInfo userData={props.userData} /> */}
        <PartyInfo partyData={props.partyData} />
        <MapInfo mapData={props.mapData} />
      {props.editMode &&
       <span id='debug-mode'>Mode: {props.editMode}</span>}
      </div>
    )
  }
}
