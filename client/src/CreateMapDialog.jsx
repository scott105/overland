import * as Overland from './overland.js';

import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function CreateMapDialog(props) {
  const [mapName, setMapName] = React.useState();
  const [ppm, setPpm] = React.useState();

  const onChangeName = (ev) => {
    setMapName(ev.target.value);
  };

  const onChangePpm = (ev) => {
    setPpm(parseFloat(ev.target.value));
  };

  const onSubmit = () => {
    Overland.createMap({
      name: mapName,
      pixelsPerMile: ppm
    });

    props.appControl.closeActiveDialog();
  };

  return (
    <Dialog open={props.open} onClose={props.appControl.closeActiveDialog} aria-labelledby="form-dialog-title" >
      <DialogTitle id="form-dialog-title">Create Map</DialogTitle>
      <form onSubmit={onSubmit}>
      <DialogContent>
        <DialogContentText>
          This world's not going to map itself.  Nope...
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Map Name"
          type="text"
          fullWidth
          onChange={onChangeName}
        />
        <TextField
          margin="dense"
          id="ppm"
          label="Pixels per Mile"
          type="text"
          fullWidth
          onChange={onChangePpm}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.appControl.closeActiveDialog} color="primary">
          Cancel
        </Button>
        <Button onClick={onSubmit} color="primary">
          Create
        </Button>
      </DialogActions>
      </form>
    </Dialog>
  );
}
