import * as Overland from './overland.js';

import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function CreatePartyDialog(props) {
  const [partyName, setPartyName] = React.useState();

  const onCreate = () => {
    Overland.createParty(partyName);
    props.appControl.closeActiveDialog();
  };

  const onChange = (ev) => {
    setPartyName(ev.target.value);
  };

  return (
    <Dialog open={props.open} onClose={props.appControl.closeActiveDialog} aria-labelledby="form-dialog-title" >
      <DialogTitle id="form-dialog-title">Create Party</DialogTitle>
      <DialogContent>
        <DialogContentText>
          So you came to party?
          We're going to have you fill out the requisite party forms, and then we'll party.
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Party Name"
          type="text"
          fullWidth
          onChange={onChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.appControl.closeActiveDialog} color="primary">
          Cancel
        </Button>
        <Button onClick={onCreate} color="primary">
          Create
        </Button>
      </DialogActions>
    </Dialog>
  );
}
