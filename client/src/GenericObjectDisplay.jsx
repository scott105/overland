import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import { Component } from 'react';

export function ObjectDisplay(props) {
  var isArray = Array.isArray(props.data);
  
  return(
    <ExpansionPanel>
      <ExpansionPanelSummary>
        {props.title}
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <List component="div" disablePadding>
          {isArray && props.data.map((val, i) => <GenericDisplay key={i} title={i} data={val}/>)}
          {!isArray && Object.keys(props.data).map(k => <GenericDisplay key={k} title={k} data={props.data[k]}/>)}
        </List>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

export default function GenericDisplay(props) {
  var thisType = typeof(props.data);

  switch(thisType) {
  case "object":
    return (
      <React.Fragment>
        <ObjectDisplay title={props.title} data={props.data} />
      </React.Fragment>
      );
    break;

  default:
    return (
      <React.Fragment>
        <ListItem>
          <ListItemText primary={`${props.title}: ${props.data}`} />
        </ListItem>
      </React.Fragment>
    )
  }
}
