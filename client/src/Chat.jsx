import React from 'react';
import { Component, useState, useEffect, useRef } from 'react';
import ReactMarkdown from 'react-markdown';

import * as Overland from './overland.js';

import Drawer from '@material-ui/core/Drawer';

import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

function Entry(props) {
  return (
    <React.Fragment>
      {props.user != props.lastUser &&
       <ReactMarkdown className='user' source={`\n---\n#### ${props.user}`} />}
      <ReactMarkdown className='message' source={props.msg} />
    </React.Fragment>
  );
}


export default function Chat(props) {
  const [messages, setMessages] = useState(props.messages || []);
  const chatContainer = useRef(null);

  useEffect(() => {
    // Connect
    Overland.registerServiceHandler('chat', (conn, response) => {
      setMessages([...messages, response.payload]);
    });
    
    return () => {
      // Disconnect
    };
  }, [props.party]);

  useEffect(() => {
    if(props.open) {
      chatContainer.current.scrollTop = chatContainer.current.scrollTopMax;
    }
  });

  var lastUser = null;

  if(!props.open) {
    return (
      <IconButton
        aria-label="chat"
        className="ChatButton"
        onClick={props.appControl.toggleChat} >
        <MenuIcon />
      </IconButton>);
  }

  return (
        <Drawer
          variant="persistent"
          anchor="right"
          open={props.open}
          className="ChatDrawer"
          classes={{paper: "ChatDrawerPaper"}}>

          <IconButton
            aria-label="chat"
            onClick={props.appControl.toggleChat}>
            <MenuIcon />
          </IconButton>
          <div className='chat-history' ref={chatContainer}>
            {true &&
             messages.map((msg, i) => {
               var last = lastUser;
               lastUser = msg.user;

               return (<Entry
                         key={msg.id}
                         user={msg.user}
                         msg={msg.msg}
                         lastUser={last}
                       />);
             })}
          </div>
        </Drawer>
);
}
