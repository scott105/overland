import { hot } from 'react-hot-loader';
import React from 'react';

import { Component } from 'react';

import _ from 'lodash';

import './App.css';
import './main.css';

import * as Overland from './overland.js';

import Cookies from 'universal-cookie';
const cookies = new Cookies();

// Components
import Auth from './Auth';
import DebugPane from './DebugPane';
import LeftPane from './LeftPane';
import MapDisplay from './map/MapDisplay';
import Chat from './Chat';

import UserInputDialog from './UserInputDialog';
import CreatePartyDialog from './CreatePartyDialog';
import CreateMapDialog from './CreateMapDialog';
import UploadFileDialog from './UploadFileDialog';
import DiscordBindingDialog from './DiscordBindingDialog';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      width: window.innerWidth,
      height: window.innerHeight,
      debugEnabled: false,
      createParty: true,
      authCompleted: false,
      userData: {},
      partyData: {},
      mapData: {},
      userView: {},
      editMode: null,
      context: {},
    };
  }

  componentDidMount() {
    var thisApp = this;
 
    window.addEventListener('resize', this.onWindowResize);

    // TODO:  Connect all data handlers
    Overland.registerServiceHandler('createparty-response', (conn, response) => {
      if(response.status == 200) {
        // TODO:  Update party list.  Should probably be re-sent on creation
        Overland.doPartyLoad(global._serverConnection,
                             { party: response.payload.id });
      }
      else {
        // TODO:  Error
      }
    });

    Overland.registerServiceHandler('clonemap-response', (conn, response) => {
      if(response.status == 200) {
        // TODO:  Update the map list somehow
        var partyData = thisApp.state.partyData;
        partyData.maps = response.payload.maps;
        thisApp.setState({partyData: partyData});

        Overland.doMapLoad(conn, {map: response.payload.created});
      }
    });

    Overland.registerServiceHandler('loadparty-response', (conn, response) => {
      // TODO:  Handle error

      var data = response.payload;

      thisApp.setState({
        partyData: data,
        activeUsers: data.active_users,
        mapData: {},
        admin: data.admin,
      });

      if(data.current_map) {
        Overland.doMapLoad(conn, {map: data.current_map});
      }
    });

    Overland.registerServiceHandler('loadmap-response', (conn, response) => {
      // TODO:  Handle error

      thisApp.setState({
        mapData: response.payload,
        userView: cookies.get(`view:${response.payload.id}`),
      });
    });

    Overland.registerServiceHandler('request-upload-response', (conn, response) => {
      return response.status == 200;
    });

    Overland.registerServiceHandler('delta', (conn, response) => {
      const currentData = thisApp.state.mapData;

      // The data broadcast system should stop this from happening, but just in case...
      if(!currentData || response.payload.map != currentData.id) {
        return;
      }

      var data = _.cloneDeep(currentData);

      // Modify necessary data
      const additions = response.payload.a;
      for(var group in additions) {
        for(var k in additions[group]) {
          // TODO:  make this able to handle hierarchical data so that individual elements of objects can be changed
          data[group][k] = additions[group][k];
        }
      }

      const deletions = response.payload.d;
      for(var group in deletions) {
        var keys = deletions[group];
        for(var i = 0; i < keys.length; ++i) {
          delete data[group][keys[i]];
        }
      }

      thisApp.setState({ mapData: data });
    });

    Overland.registerServiceHandler('party-heartbeat-response', (conn, response) => {
      thisApp.setState({ activeUsers: response.payload.active_users });
    });

    Overland.registerServiceHandler('party-switchmap', (conn, response) => {
      Overland.doMapLoad(conn, {map: response.payload.map});
    });

  }

  componentWillUnmount() {
    // TODO:  Possibly disconnect handlers

    window.removeEventListener('resize', this.onWindowResize);
  }

  onAuthenticationCompleted = (data) => {
    this.setState({
      ...data,
      authCompleted: true,
      partyData: {},
      mapData: {}
    });

    // HACK:  Global
    global._serverConnection = data.connection;

    // Attempt to load any query string specified party or store info on last loaded. 
    var partyToLoad = global._queryParams.get('party');

    if(!partyToLoad) {
      partyToLoad = data.userData.last_party;
    }

    if(partyToLoad) {
      Overland.doPartyLoad(global._serverConnection,
                           { party: partyToLoad });
    }

    if(global._queryParams.get('bindchannel')) {
      this.setState({ activeDialog: 'discord_binding' });
    }
  }

  //
  appControl = {
    openDialog: (name) => {
      this.setState({ activeDialog: name });
    },

    closeActiveDialog: () => {
      this.setState({ activeDialog: null });
    },

    promptForInput: (req) => {
      this.setState({ activeDialog: null,
                      userInputRequest: req
                    });
    },

    clearInputPrompt: () => {
      this.appControl.promptForInput(null, null);
      this.appControl.closeActiveDialog();
    },

    logout: () => {
      cookies.remove('token');
      // TODO:  Tell server to invalidate our token and disconnect from the websocket
      this.setState({ authCompleted: false });
    },

    setEditMode: (mode) => {
      this.setState({ editMode: mode });
    },

    updateContext: (data) => {
      this.setState({
        context: {
          ...this.state.context,
          ...data,
        }
      });
    },

    getContext: () => {
      return this.state.context;
    },

    toggleLeftPane: () => {
      this.setState({ leftPaneOpen: !this.state.leftPaneOpen });
    },

    toggleChat: () => {
      this.setState({ chatOpen: !this.state.chatOpen });
    },
  }

  onWindowResize = () => {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  render() {
    var state = this.state;

    if(!state.authCompleted) {
      return <Auth onSuccess={this.onAuthenticationCompleted} />;
    }
 
    return (
      <div className='App'>
        <DebugPane
          userData={state.userData}
          partyData={state.partyData}
          mapData={state.mapData}
          editMode={state.editMode} />

        <LeftPane
          open={state.leftPaneOpen}
          admin={state.admin}
          userData={state.userData}
          partyData={state.partyData}
          mapData={state.mapData}
          appControl={this.appControl} />

        <MapDisplay
          editMode={state.editMode}
          admin={state.admin}
          mapData={state.mapData}
          partyData={state.partyData}
          activeUsers={state.activeUsers}
          userView={state.userView}
          width={state.width}
          height={state.height}
          appControl={this.appControl} />

        {state.partyData && state.partyData.id &&
         <Chat
           key={state.partyData.id}
           open={state.chatOpen}
           admin={state.admin}
           party={state.partyData.id}
           messages={state.partyData.chat.slice().reverse()}
           appControl={this.appControl} />}

        {this.state.userInputRequest &&
         <UserInputDialog
           type={this.state.userInputRequest.type}
           title={this.state.userInputRequest.title}
           field={this.state.userInputRequest.field}
           initialValue={this.state.userInputRequest.initial}
           onCompleted={this.state.userInputRequest.callback}
           appControl={this.appControl} />
        }

        <CreatePartyDialog open={state.activeDialog === "create_party"} appControl={this.appControl} />
        <CreateMapDialog open={state.activeDialog === "create_map"} appControl={this.appControl} />
        <UploadFileDialog open={state.activeDialog === "upload_file"} appControl={this.appControl} userData={state.userData}/>
        <DiscordBindingDialog open={state.activeDialog === "discord_binding"} appControl={this.appControl} userData={state.userData}/>
      </div>
    );
  }
}

/////
export default hot(module)(App);
