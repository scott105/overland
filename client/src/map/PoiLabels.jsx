import React, { Component } from 'react';
import Konva from 'konva';
import { Stage, Layer, FastLayer, Group, Label, Tag, Text, Image } from 'react-konva';

export default function PoiLabels(props) {
  return (
    <React.Fragment>
      {Object.keys(props.poi).map(name => {
        var poi = props.poi[name];

        const onEnter = () => {
          props.appControl.updateContext({ poi: name });
        };

        const onLeave = () => {
          props.appControl.updateContext({ poi: null });
        };

        if(props.admin || !poi.hidden) {
          return (
            <Label
              key={name}
              opacity={0.5}
              x={poi.x * props.ppm}
              y={poi.y * props.ppm}
            /* TODO: Port label placement logic; until then, highly unscientific calculation */
              offsetX={name.length * 5}
              offsetY={30}>
              <Tag
                fill={poi.hidden ? 'red' : 'black'}
                shadowColor='black'
                shadowBlur={10}
                shadowOffset={{x: 10, y: 10}}
                shadowOpacity={0.5} />
              <Text
                text={name}
                padding={5}
                align='center'
                fontSize={30}
                fontFamily='Calibri'
                fontSize={18}
                fill='white'
                onMouseEnter={onEnter}
                onMouseLeave={onLeave} />
            </Label>
          );
        }
      })}
    </React.Fragment>
  );
}
