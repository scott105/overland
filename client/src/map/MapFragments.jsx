import React, { Component } from 'react';
import Konva from 'konva';
import { Stage, Layer, FastLayer, Group, Label, Tag, Text, Image } from 'react-konva';

import useImage from 'use-image';

function FragmentImage(props) {
  const [image] = useImage(props.frag);
  return <Image
           image={image}
           x={props.x}
           y={props.y}
           scaleX={props.scale}
           scaleY={props.scale}
         />;
}

export default function MapFragments(props) {
  return (
    <React.Fragment>
      {Object.keys(props.fragments).map(name => {
        var frag = props.fragments[name];
        var fragScale = props.ppm / (frag.pixelspermile || frag.pixelsPerMile);

        return <FragmentImage
                     key={name}
                     frag={frag.img}
                     x={frag.x * props.ppm}
                     y={frag.y * props.ppm}
                     scale={fragScale} />;
      })}
    </React.Fragment>
  );
}
