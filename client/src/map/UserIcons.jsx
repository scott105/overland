import React, { Component, useState, useEffect, useRef } from 'react';
import Konva from 'konva';
import { Circle, Rect, Label, Tag, Text, Image } from 'react-konva';

import useImage from 'use-image';

function UserPortrait(props) {
  const iconRef = useRef(null);
  const [image] = useImage(props.avatar);
  var scale = image ? props.radius * 2 / image.width : 1.0;

  useEffect(() => {
    iconRef.current.to({
      radius: props.radius,
      fillPatternScaleX: scale,
      fillPatternScaleY: scale,
      fillPatternOffsetX: -props.radius / scale,
      fillPatternOffsetY: -props.radius / scale,
      duration: 0.5,
    });
  });

  return <React.Fragment>
           <Circle
             ref={iconRef}
             x={props.x}
             y={props.y - props.radius - 4}
             radius={1}
             fillPatternImage={image}
             fillPatternScaleX={1}
             fillPatternScaleY={1}
             fillPatternOffsetX={1}
             fillPatternOffsetY={1}
             stroke={"rgb(255, 255, 255, 120)"}
             strokeWidth={2}
             shadowColor={'black'}
             shadowBlur={8}
             shadowOffset={{x: 5, y: 5}}
             shadowOpacity={0.5}
           />

           <Label
             x={props.x}
             y={props.y}
             offsetX={props.alias.length * 4.25}
             offsetY={30}>
             <Text
               text={props.alias}
               padding={5}
               shadowColor='black'
               shadowBlur={3}
               shadowOffset={{x: 0, y: 0}}
               shadowOpacity={0.7}
               stroke={'black'}
               strokeWidth={1}
               align='center'
               fontFamily='Calibri'
               fontSize={props.fontSize}
               fontStyle={'bold'}
               fill={'white'} />
           </Label>
         </React.Fragment>;
}

export default function UserIcons(props) {
  const spacing = props.radius * 4;
  
  return (
    <React.Fragment>
      {props.activeUsers.map((name, i) => {
        var alias = props.partyData.aliases[name];
        var avatar = props.partyData.avatars[name];

        return (
          <UserPortrait
            key={name}
            avatar={avatar}
            alias={alias}
            x={props.x + props.radius + 30 + i * spacing}
            y={props.height}
            radius={props.radius}
            fontSize={props.fontSize}
        />);
      })}
    </React.Fragment>
  );
}
