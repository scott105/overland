import React, { Component, useEffect, useState, useRef } from 'react';
import Konva from 'konva';
import { Circle, Ring, Rect, Label, Tag, Text, Image, Line } from 'react-konva';

import useImage from 'use-image';

import * as Overland from '../../overland.js';

function PoiIcon(props) {
  const ix = (props.icon % props.cols) * props.size;
  const iy = Math.floor(props.icon / props.cols) * props.size;

  const dragEnd = (e) => {
    const obj = e.target;
    const pos = obj.position();

    if(props.onPlacement) {
      props.onPlacement(props.icon, pos);
    }

    obj.position({ x: props.x, y: props.y });
  };

  return (
    <Rect
      x={props.x}
      y={props.y}
      offsetX={props.size * 0.5}
      offsetY={props.size * 0.5}
      draggable={true}
      onDragEnd={dragEnd}
      stroke={'white'}
      strokeWidth={1}
      width={props.size}
      height={props.size}
      fillPatternImage={props.atlas}
      fillPatternOffset={{ x: ix, y: iy }}
      cornerRadius={10}
      onMouseEnter={(e) => { e.target.setStroke('yellow'); }}
      onMouseLeave={(e) => { e.target.setStroke('white'); }}
    />
  );

}

export default function ToolBox(props) {
  const boxRef = useRef(null);
  const [atlas] = useImage(props.atlas);

  // TODO: Should this be in its own folder?
  // .Allow for different toolbox content based on editMode

  var icons = [];
  var size = 64;
  var imageWidth = 1024;
  var perAtlasRow = 16;

  var perToolRow = Math.floor((props.width - 50) / (size + 4));

  for(var i = 0; i < 32; i++) {
    var x = (i % perToolRow) * (size + 4);
    var y = Math.floor(i / perToolRow) * (size + 4);

    const onPlacement = (icon, pos) => {
      // Drag off of palette
      if(pos.x > props.width) {
        var world = props.posToWorld(pos);

        var key = 'NEW PLACE';
        for(var i = 2; props.mapData.poi[key]; i++) {
          key = `NEW PLACE ${i}`;
        }

        Overland.sendMapDelta(props.mapData.id, {
          poi: {
            [key]: {
              icon: icon,
              x: world.x / props.ppm,
              y: world.y / props.ppm,
              hidden: true,
            }
          }
        });
      }
    };

    icons.push(
      <React.Fragment key={'tool:box' + i}>
        <Rect
          key={'icon:backing:' + i}
          x={x + 50}
          y={y + 100}
          offsetX={size * 0.5}
          offsetY={size * 0.5}
          width={size}
          height={size}
          cornerRadius={10}
          fillRadialGradientStartPoint={{x: 0, y: size * 0.5}}
          fillRadialGradientStartRadius={0}
          fillRadialGradientEndRadius={size}
          fillRadialGradientColorStops={[0, 'cyan', 1, 'slategray']}
        />
        <PoiIcon
          key={'icon:' + i}
          atlas={atlas}
          cols={perAtlasRow}
          size={size}
          icon={i}
          x={x + 50}
          y={y + 100}
          onPlacement={onPlacement}
        />
      </React.Fragment>
    );
  }

  return (
    <React.Fragment>
      <Rect
        ref={boxRef}
        width={props.width}
        height={props.height}
        fill={'slategray'}
        stroke={'black'}
        strokeWidth={2}
        shadowBlur={10}
      />

      {icons}
    </React.Fragment>
  );
}
