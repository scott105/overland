import React, { Component } from 'react';
import Konva from 'konva';
import { RegularPolygon, Group, Label, Tag, Text } from 'react-konva';

import * as Honeycomb from 'honeycomb-grid';

function _hexesBetween(firstHex, lastHex) {
  const distance = firstHex.distance(lastHex);
  const step = 1.0 / Math.max(distance, 1);
  let hexes = [];

  for (let i = 0; i <= distance; i++) {
    const hex = firstHex
          .nudge()
          .lerp(lastHex.nudge(), step * i)
          .round();
    hexes.push(hex);
  }

  return hexes;
}

function _hexes(key, hexSize, scale, poly, hexes) {
  return hexes.map(hex => {
    var hexPos = hex.toPoint();

    return (
      <RegularPolygon
        key={key + hex}
        x={hexPos.x + hexSize}
        y={hexPos.y + hexSize}
        sides={6}
        radius={hexSize}
        fill={poly.fill}
        stroke={poly.color || 'gray'}
        strokeWidth={2 / scale}
        opacity={poly.opacity || 0.8} />
    );
  });
}

function _getPath(Grid, hexSize, props) {
  if(!props.waypoints) {
    return;
  }

  // NOTE:  This doesn't support doubling back on a path.
  // .Conceptually, this is more of a measurement tool

  var points = props.waypoints.slice();
  points.push(props.pos);

  var path = [];

  for(var i = 1; i < points.length; ++i) {
    var startPoint = points[i - 1];
    var endPoint = points[i];

    var startHex = Grid.pointToHex(startPoint.x, startPoint.y);
    var endHex = Grid.pointToHex(endPoint.x, endPoint.y);

    path = path.concat(_hexesBetween(startHex, endHex));
  }

  const unique = path.reduce((acc, current) => {
    const x = acc.find(item => item.x === current.x && item.y === current.y);
    if (!x) {
      return acc.concat([current]);
    } else {
      return acc;
    }
  }, []);

  const hexPath = _hexes('path', hexSize, props.scale,
                         { color: 'black',
                           fill: props.path_color,
                           opacity: 0.5 },
                         unique);

  return hexPath;
}

function _pathLabel(props, miles, path) {
  if(path && path.length > 1) {
    const scale = 1.0 / props.scale;
    const fontSize = 30 * scale;
    const length = path.length - 1;

    return (
      <Label
        key={'pathlabel'}
        opacity={0.5}
        x={props.pos.x}
        y={props.pos.y - fontSize * 2}>
        <Tag
          fill='black'
          shadowColor='black'
          shadowBlur={10}
          shadowOffset={{x: 10 * scale, y: 10 * scale}}
          shadowOpacity={0.5} />
        <Text
          text={`(${length}) ${length * miles} mi.`}
          padding={5 * scale}
          align='center'
          fontSize={fontSize}
          fontFamily='Calibri'
          fill='white' />
      </Label>
    );
  }
}

export default function HexCursor(props) {
  if(props.zoom >= props.zoomMax) {
    return null;
  }

  const level = props.zoomMax - props.zoom;
  const zoomColor = props.colors[level];
  const scalingFactor = level * 0.25;
  const hexMiles = props.miles * scalingFactor;

  const hexSize = hexMiles * 0.5 * props.ppm;

  const Hex = Honeycomb.extendHex({ size: hexSize });
  const Grid = Honeycomb.defineGrid(Hex);

  var centerHex = Grid.pointToHex(props.pos.x, props.pos.y);

  var area = Grid.hexagon({
    radius: props.range,
    center: centerHex
  });

  var pathHexes = _getPath(Grid, hexSize, props);

  return (
    <React.Fragment>
      { _hexes('main', hexSize, props.scale,
               { color: zoomColor,
                 fill: '#fff3',
                 opacity: 0.8 },
               area)}
      { pathHexes }
      { _pathLabel(props, hexMiles, pathHexes) }
    </React.Fragment>
  );
}
