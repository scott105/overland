import React, { Component, useState, useRef, useEffect } from 'react';
import Konva from 'konva';
import { Rect } from 'react-konva';

import useImage from 'use-image';

import * as Overland from '../overland.js';

export default function PoiIcons(props) {
  const [atlas] = useImage(props.atlas);

  const onDragEnd = (key, e) => {
    if(!props.edit) {
      return;
    }

    var data = props.poi[key];

    if(!data) {
      return;
    }

    data.x = (e.target.x() - (props.drawOffsetX || 0)) / props.ppm;
    data.y = (e.target.y() - (props.drawOffsetY || 0)) / props.ppm;

    Overland.sendMapDelta(props.mapId, {
      poi: {
        [key]: data
      }
    });
  };

  return (
    <React.Fragment>
      {Object.keys(props.poi).map(name => {
        var poi = props.poi[name];

        const onEnter = () => {
          props.appControl.updateContext({ poi: name });
        };

        const onLeave = () => {
          props.appControl.updateContext({ poi: null });
        };

        if(props.admin || !poi.hidden) {
          return (
            <Rect
              key={name}
              fillPatternImage={atlas}
              draggable={props.edit}
              onDragEnd={(e) => {onDragEnd(name, e);}}
              onMouseEnter={onEnter}
              onMouseLeave={onLeave}
              x={poi.x * props.ppm + (props.drawOffsetX || 0)}
              y={poi.y * props.ppm + (props.drawOffsetY || 0)}
              width={props.tileWidth}
              height={props.tileHeight}
              fillPatternOffset={{
                x: (poi.icon % 16) * props.tileHeight,
                y: Math.floor(poi.icon / 16) * props.tileWidth
              }}

            />
          );
        }
      })}
    </React.Fragment>
  );
}
