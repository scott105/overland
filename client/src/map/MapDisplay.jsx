import React, { Component } from 'react';
import Konva from 'konva';
import { Stage, Layer, FastLayer, Group, Label, Tag, Text, Image } from 'react-konva';

import MapFragments from './MapFragments';
import PoiIcons from './PoiIcons';
import PoiLabels from './PoiLabels';
import HexCursor from './HexCursor';
import UserIcons from './UserIcons';
import RadialMenu from './RadialMenu';
import ToolBox from './tools/ToolBox';

import Cookies from 'universal-cookie';
const cookies = new Cookies();

import * as Overland from '../overland.js';

// TODO:  Functions for converting between coordinate systems should be here
// .ui<->world, world<->map
// .Binding layers to be in the same position (or relative positions, etc.) should end up being handled by view position data changing here

export default class MapDisplay extends Component {
  constructor(props) {
    super(props);

    this.mapRef = React.createRef();
    this.stageRef = React.createRef();

    this.state = {
      scale: 1,
      offset: {
        x: 0,
        y: 0
      },
      cursor: {
        x: 0,
        y: 0,
      },
      hex_size: 24,
      hex_radius: 1,
      hex_colors: [null, 'cyan', 'magenta', 'yellow', 'white'],
      hex_zoom: 0,
      hex_zoom_max: 4,
      mode: 0,
      radialActive: false
    };
  }

  posToWorld = (pos) => {
    var ppm = this.props.mapData.pixelsPerMile;
    var map = this.mapRef.current;

    var screenToWorld = map.getTransform().copy().invert();

    return screenToWorld.point(pos);
  }

  pointerToWorld = () => {
    const stage = this.stageRef.current;
    return this.posToWorld(stage.getPointerPosition());
  }

  addWaypoint = (point) => {
    var waypoints = this.state.path_waypoints || [];

    waypoints.push(point);

    this.setState({
      path_waypoints: waypoints
    });
  }

  clearWaypoints = () => {
    this.setState({ path_waypoints: null });
  }

  handleMouseMove = e => {
    var world = this.pointerToWorld();

    this.setState({
      cursor: {
        x: world.x,
        y: world.y,
      }
    });

    // Waypoints
    if(!e.evt.altKey) {
      this.clearWaypoints();
    }
  }

  handleMouseDown = e => {
    // Waypoints
    if(e.evt.altKey) {
      var world = this.pointerToWorld();
      this.addWaypoint(world);
    }
  }

  handleMouseUp = e => {
    // Waypoints
    if(!e.evt.altKey) {
      this.clearWaypoints();
    }
  }

  handleMouseWheel = e => {
    if(e.evt.shiftKey) {
      this.handleHexZoom(e);
    }
    else {
      this.handleMapZoom(e);
    }
  }

  setHexSize = size => {
    this.setState({ hex_size: size });
    this.setZoomLevel(this.state.hex_zoom);
  }

  setZoomLevel = level => {
    var actualLevel = Math.max(0, Math.min(this.state.hex_zoom_max, level));
    this.setState({ hex_zoom: actualLevel });
  }

  handleHexZoom = e => {
    var delta = Math.max(-1, Math.min(1, e.evt.deltaY || e.evt.wheelDelta));
    this.setZoomLevel(this.state.hex_zoom - delta);
  }

  handleMapDrag = e => {
    // Note: To stop propagation of nested drag events
    if(e.target != this.mapRef.current) {
      return;
    }

    var mapId = this.props.mapData.id;
    var cookieName = `view:${mapId}`;
    var view = cookies.get(cookieName) || {};

    view.x = e.target.x();
    view.y = e.target.y();

    var map = this.mapRef.current;
    view.zoom = map.scale().x;

    cookies.set(cookieName, view);
  }

  handleMapZoom = e => {
    // HACK:  The scaling and position are manually applied here to make the default dragging behavior work
    // .the state being modified here is more to kick the render, but the scaling might be useful for hex grid display
    var map = this.mapRef.current;
    var stage = this.stageRef.current;

    var scale = map.scale().x;
    var mouseZoomScale = -0.125;
    var delta = Math.max(-1, Math.min(1, e.evt.deltaY || e.evt.wheelDelta)) * mouseZoomScale;

    var newScale = Math.max(0.0125, scale + delta * scale);

    var pointer = stage.getPointerPosition();
    var screenToWorld = map.getTransform().copy().invert();
    var worldCenter = screenToWorld.point(pointer);

    map.scale({
      x: newScale,
      y: newScale
    });

    newScale = map.scale().x;
    screenToWorld = map.getTransform().copy().invert();
    var newCenter = screenToWorld.point(pointer);

    var offset = {
      x: (newCenter.x - worldCenter.x) * newScale,
      y: (newCenter.y - worldCenter.y) * newScale
    };

    map.move(offset);

    this.setState({
      scale: newScale,
      offset: {
        x: map.position().x + offset.x,
        y: map.position().y + offset.y
      }
    });

    // HACK:  Manually kick render
    stage.draw();
  }

  handleKeyDown = (e) => {
    if(e.key == ' ') {
      if(!e.repeat) {
        this.setState({
          radialActive: !this.state.radialActive,
          currentRadialAction: null
        });
      }
    }
  }

  handleKeyUp = (e) => {
    if(e.key == ' ') {
      if(this.state.radialActive && this.state.currentRadialAction) {
        this.state.currentRadialAction();
      }

      this.setState({ radialActive: false });
    }
  }

  handleContextMenu = (e) => {
    e.evt.preventDefault();
    this.setState({ radialActive: !this.state.radialActive });
  }

  handleRadialSelection = (action) => {
    this.setState({ currentRadialAction: action });
  }

  updatePoi = (key, data) => {
    const existing = this.props.mapData.poi[key];

    if(!existing) {
      return;
    }

    Overland.sendMapDelta(this.props.mapData.id, {
      poi: {
        [key]: { ...existing, ...data }
      }
    });
  };

  deletePoi = (key) => {
    Overland.sendMapDelta(this.props.mapData.id, null, {poi: [key], annotations: [key]});
  };

  renamePoi = (key, newKey) => {
    // Note:  No real option than to remove the old one and add the new
    if(newKey && newKey != '' && newKey != key) {
      var cleanKey = newKey;
      for(var i = 2; this.props.mapData.poi[cleanKey]; i++) {
        cleanKey = `${newKey} ${i}`;
      }

      const poiData = this.props.mapData.poi[key];
      const annotation = this.props.mapData.annotations[key];

      Overland.sendMapDelta(this.props.mapData.id, {
        poi: { [cleanKey]: poiData },
        annotations: annotation && { [cleanKey]: annotation }
      }, {
        poi: [key],
        annotations: [key]
      });
    }
  };

  render() {
    var state = this.state;
    var mapData = this.props.mapData;
    var userView = this.props.userView || {};

    if(!mapData || !mapData.fragments) {
      return null;
    }

    var admin = this.props.admin;
    var ppm = mapData.pixelsPerMile;
    var editMode = this.props.editMode;

    const radialUi = {
      label: 'UI',
      menu: {
        // n: { label: 'Party', run: () => {} },
        e: { label: 'Chat', run: this.props.appControl.toggleChat },
        // s: { label: 'Handouts', run: () => {} },
        w: { label: 'Menu', run: this.props.appControl.toggleLeftPane },
      }
    };

    const radialMap = {
      label: 'Map',
      menu: {
        e: admin && { label: 'Move Party Here',
                      run: () => {
                        Overland.sendToService(global._serverConnection, {
                          type: 'party-switchmap',
                          payload: {
                            map: this.props.mapData.id
                          }
                        });
                      }
                    },
        w: { label: 'Base Movement',
             run: () => this.props.appControl.promptForInput({
               type: 'number',
               title: 'Base Hex Size',
               field: 'Size: ',
               callback: (num) => this.setHexSize(num),
               initial: this.state.hex_size,
             })
           }
      }
    };

    const radialDefault = {
      // n: { label: 'Ping' },
      e: admin && { label: 'POI Mode', run: () => this.props.appControl.setEditMode('poi') },
      // s: { label: 'Fragment Mode' },
      w: { label: 'View Mode', run: () => this.props.appControl.setEditMode(null) },
      ne: radialUi,
      sw: radialMap,
    };
    
    const createRadialMenuData = () => {
      const context = this.props.appControl.getContext();
      return {
        ...radialDefault,
        ...(context.poi && admin ? { 'nw': {
          label: 'POI',
          menu: {
            n: editMode == 'poi' && {
              label: 'Rename', run: () => this.props.appControl.promptForInput({
                type: 'string',
                title: `Rename '${context.poi}'`,
                field: 'Name: ',
                callback: (str) => this.renamePoi(context.poi, str),
                initial: context.poi,
              })
            },

            e: { label: 'Show', run: () => this.updatePoi(context.poi, { hidden: false }) },
            s: editMode == 'poi' && {
              label: 'Delete', run: () => this.props.appControl.promptForInput({
                type: 'yesno',
                title: 'Confirm Deletion',
                callback: (yes) => {
                  if(yes) {
                    this.deletePoi(context.poi);
                  }
                },
              }),
            },
            w: { label: 'Hide', run: () => this.updatePoi(context.poi, { hidden: true }) },
          }
        } } : null)
      };
    };

    const toolboxWidth = Math.max((64 + 4) * 2 + 50,
                                  Math.min((64 + 4) * 4 + 50,
                                           this.props.width * 0.25));

    return (
      <div
        tabIndex={1}
        onKeyDown={this.handleKeyDown}
        onKeyUp={this.handleKeyUp} >

        <Stage
          ref={this.stageRef}
          className='MapCanvas'
          width={this.props.width}
          height={this.props.height}
          onWheel={this.handleMouseWheel}
          onMouseMove={this.handleMouseMove}
          onMouseDown={this.handleMouseDown}
          onMouseUp={this.handleMouseUp}
          onContextmenu={this.handleContextMenu}
        >
          <Layer
            ref={this.mapRef}
            key='map-layer'
            draggable={true}
            onDragEnd={this.handleMapDrag}
            x={userView.x || 0}
            y={userView.y || 0}
            scaleX={userView.zoom || 1}
            scaleY={userView.zoom || 1}
          >

            <MapFragments admin={admin} ppm={ppm} fragments={mapData.fragments} />
            { state.hex_size > 0 &&
              editMode != 'poi' &&
              !state.radialActive &&
              <HexCursor
                pos={state.cursor}
                scale={state.scale}
                waypoints={state.path_waypoints}
                ppm={ppm}
                miles={state.hex_size}
                zoom={state.hex_zoom}
                zoomMax={state.hex_zoom_max}
                range={state.hex_radius}
                colors={state.hex_colors}
                path_color={'cyan'}/>
            }

            <PoiIcons
              admin={admin} ppm={ppm} poi={mapData.poi}
              mapId={mapData.id}
              edit={admin && editMode == 'poi'}
              atlas='static/overland-icons.png'
              tileWidth={64}
              tileHeight={64}
              drawOffsetX={-32}
              drawOffsetY={-32}
              appControl={this.props.appControl} />

            <PoiLabels
              admin={admin}
              ppm={ppm}
              poi={mapData.poi}
              appControl={this.props.appControl} />

          </Layer>
          <Layer
            key='static-overlay-layer'
          >
            <UserIcons
              partyData={this.props.partyData}
              activeUsers={this.props.activeUsers}
              height={this.props.height}
              x={toolboxWidth}
              radius={35}
              fontSize={18}
            />

            { editMode &&
              <ToolBox
                key='toolbox'
                mapData={mapData}
                ppm={ppm}
                editMode={editMode}
                height={this.props.height}
                width={toolboxWidth}
                atlas={'static/overland-icons.png'}
                posToWorld={(pos) => { return this.posToWorld(pos); }}
                appControl={this.props.appControl}
              />
            }

            { state.radialActive &&
              <RadialMenu
                key='radial-menu'
                stageRef={this.stageRef}
                radius={15}
                height={this.props.height}
                width={this.props.width}
                itemWidth={100}
                itemHeight={40}
                itemFontSize={15}
                iconSize={25}
                itemOffset={10}
                onCompleted={() => {this.setState({ radialActive: false });}}
                onSelection={this.handleRadialSelection}
                data={createRadialMenuData()}
              />
            }
          </Layer>
        </Stage>
      </div>
    );
  }
}
