import React, { Component, useEffect, useState } from 'react';
import Konva from 'konva';
import { Circle, Ring, Rect, Label, Tag, Text, Image, Line } from 'react-konva';

import useImage from 'use-image';

const _itemLayout = {
  n: { x: -0.5, y: -1 },
  ne: { x: 0, y: -0.8 },
  e: { x: 0, y: -0.5 },
  se: { x: 0, y: -0.2 },
  s: { x: -0.5, y: 0 },
  sw: { x: -1, y: -0.2 },
  w: { x: -1, y: -0.5 },
  nw: { x: -1, y: -0.8 },
}

const _dirOrder = ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw'];
const _itemOrder = [ 'ne', 'se', 'sw', 'nw',
                     'n', 'e', 's', 'w' ];

function _normalize(v) {
  const magnitude = _magnitude(v);
  return {x: v.x / magnitude,
          y: v.y / magnitude}
}

function _dot(v1, v2) {
  return v1.x * v2.x + v1.y * v2.y;
}

function _magnitude(v) {
  return Math.sqrt(v.x * v.x + v.y * v.y);
}

function _sub(v1, v2) {
  return {x: v1.x - v2.x,
          y: v1.y - v2.y}
}

function _rotate(v, angle) {
  const cos = Math.cos(angle);
  const sin = Math.sin(angle);
  return {x: v.x * cos - v.y * sin,
          y: v.x * sin + v.y * cos}
}

const _itemDirections = (() => {
  var angle = 45 * Math.PI / 180;
  var dir = {x: 0, y: -1}
  var directions = {}

  for(var i = 0; i < 8; i++) {
    directions[_dirOrder[i]] = dir;
    dir = _rotate(dir, angle);
  }

  return directions;
})();

export default function RadialMenu(props) {
  const [pos, setPos] = useState(null);
  const [data, setData] = useState(props.data);
  const [selection, setSelection] = useState(null);
  const [itemDirections, setItemDirections] = useState(null);

  useEffect(() => {
    const cursorPos = props.stageRef.current.getPointerPosition();
    setPos(cursorPos);
  }, []);

  const updateSelection = (item) => {
    if(props.onSelection) {
      props.onSelection(item && item.run);
    }
  }

  const onBackingMove = (e) => {
    var mouse = { x: e.evt.clientX, y: e.evt.clientY };
    var delta = _sub(mouse, pos);

    if(_magnitude(delta) < props.radius) {
      setSelection(null);
      updateSelection(null);
      return;
    }
    
    var mouseDir = _normalize(delta);

    _dirOrder.map((idx) => {
      var dir = _itemDirections[idx];
      if(dir && _dot(dir, mouseDir) > 0.95) {
        var item = data[idx];
        if(!item) {
          setSelection(null);
          updateSelection(null);
        }
        else if(item.menu) {
          setPos(mouse);
          setData(item.menu);
        }
        else {
          setSelection(idx);
          updateSelection(item);
        }
      }
    });
  }

  const onClick = (e) => {
    if(selection) {
      var item = data[selection];
      if(item && item.run) {
        item.run();
        updateSelection(null);
      }
    }
    props.onCompleted();
  }

  if(!pos || !data) {
    return null;
  }

  return (
    <React.Fragment>
      <Rect
        key={'radial-backing'}
        width={props.width}
        height={props.height}
        onClick={onClick}
        onMouseMove={onBackingMove}
      />
      <Ring
        x={pos.x}
        y={pos.y}
        innerRadius={props.radius}
        outerRadius={props.radius + props.iconSize}
        stroke={"rgb(50, 50, 50, 120)"}
        strokeWidth={2}
        fill={'cyan'}
        opacity={0.5}
        shadowBlur={10}
        listening={false}
      />

      { _itemOrder.map((i) => {
        var item = data[i];
        var layout = _itemLayout[i];
        var dir = _itemDirections[i];

        if(!item) {
          return null;
        }

        var isSelected = selection && selection == i;
        var isMenu = item.menu != undefined;

        var itemPos = {
          x: pos.x + dir.x * (props.radius + props.iconSize * 0.3 + (props.itemOffset || 0)) + props.itemWidth * layout.x,
          y: pos.y + dir.y * (props.radius + props.iconSize * 0.3 + (props.itemOffset || 0)) + props.itemHeight * layout.y,
        }

        const onMouse = (e) => {
          if(!isMenu) {
            setSelection(i);
            updateSelection(item);
          }
        }

        return (
          <React.Fragment key={'item' + i}>
            <Rect
              key={`${i}-backing`}
              x={itemPos.x}
              y={itemPos.y}
              width={props.itemWidth}
              height={props.itemHeight}
              cornerRadius={isMenu ? 5 : 15}
              fill={isMenu ? 'gray' : 'lightgray'}
              onClick={onClick}
              opacity={isSelected ? 0.8 : 0.7}
              shadowBlur={10}
              stroke={isSelected ? 'yellow' : null}
            />
            <Text
              key={'label' + i}
              x={itemPos.x}
              y={itemPos.y}
              text={item.label}
              width={props.itemWidth}
              height={props.itemHeight}
              padding={5}
              align={'center'}
              verticalAlign={'middle'}
              fontFamily={'Calibri'}
              fontSize={props.itemFontSize}
              fontStyle={isMenu ? 'italic' : 'normal'}
              fill={isMenu ? 'white' : 'black'}
              onClick={onClick}
              onMouseOver={!isMenu && onMouse}
              onMouseMove={isMenu && onBackingMove}
            />
          </React.Fragment>
        );
      })
      }

      { _itemOrder.map((i) => {
        var item = data[i];

        if(!item || !item.icon) {
          return null;
        }

        var dir = _itemDirections[i];
        var isSelected = selection && selection == i;
        var isMenu = item.menu != undefined;

        var iconPos = {
          x: pos.x + dir.x * (props.radius + 5),
          y: pos.y + dir.y * (props.radius + 5)
        }

        return (
          <Rect
            key={'icon' + i}
            x={iconPos.x}
            y={iconPos.y}
            offsetX={props.iconSize * 0.5}
            offsetY={props.iconSize * 0.5}
            width={props.iconSize}
            height={props.iconSize}
            opacity={0.8}
            fill={'gray'}
            stroke={isSelected ? 'cyan' : 'white'}
            strokeWidth={1}
            cornerRadius={3}
          />
        )})}

    </React.Fragment>
  );
}
