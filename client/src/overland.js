import Cookies from './js.cookie.js'

// Modes
var Modes = Object.freeze({VIEW: 0,
                           POI: 2,
                           FRAG: 3})

const _heartbeat = 10000;
var _serverConnection;
var _logAll = false;
      
//
var _queryParams;
var _mode = Modes.VIEW;
var _serviceHandlers = {}
var _menuHandlers = {}
var _serviceCallbacks = {}
var _modeCallbacks = {}

var _mapData;
var _userData;
var _partyData;
var _imageSources = {};

//
var _markdown = createMarkdownConverter();

var _konvaStage;
var _activeUsersStage;
var _serverConnection;

var _lastChatUser;
var _currentFragment;

export function registerServiceHandler(type, handler) {
  _serviceHandlers[type] = handler;
}

registerServiceHandler('broadcast', function(conn, response) {
  var data = response.payload;
  data.status = 200;

  if(_logAll) {
    console.log(data);
  }
  
  // Dispatch
  var handler = _serviceHandlers[data.type];
  if(handler) {
    handler(conn, data);
  }
  else {
    // TODO:  Error
  }
});

function registerMenuHandler(path, handler) {
  _menuHandlers[path] = handler;
}

function registerModeHandler(mode, start, end) {
  _modeCallbacks[mode] = {start: start, end: end}
}

function changeMode(mode) {
  if(mode == _mode) {
    return;
  }
  
  console.log(`MODE: ${_mode} => ${mode}`);

  var cb = _modeCallbacks[_mode];

  if(cb && cb.end) {
    cb.end();
  }

  _mode = mode;
  cb = _modeCallbacks[mode];

  if(cb && cb.start) {
    cb.start();
  }
}

export function loadAccessToken() {
  return Cookies.getJSON('token');
}

export function saveAccessToken(token) {
  if(token) {
    Cookies.set('token', token);
  }
}

export function clearAccessToken() {
  Cookies.remove('token');
}

function createMarkdownConverter() {
  // var converter = new Markdown.Converter({
  //   asteriskIntraWordEmphasis: true
  // });
  // converter.hooks.chain("preConversion", (text) => {
  //   return $('<div>').text(text).html();
  // });
  // converter.hooks.chain("preSpanGamut", (text) => {
  //   return text.replace(/__(.*?)__/g, "<u>$1</u>");
  // });
  // converter.hooks.chain("preSpanGamut", (text) => {
  //   return text.replace(/~T~T(.*?)~T~T/g, "<s>$1</s>");
  // });
  // converter.hooks.chain("postConversion", (text) => {
  //   return text.replace(/\n/g, "<br/>");
  // });

  // return converter;
}

//
// TODO:  Provide some momentum to the movement
// .That will require some sort of tweening or something where this will set the target location instead of actually doing the transform
// ..Depending on the speed of the render function, that may be a problem.

function rotateVec(vec, angle) {
  var radians = (Math.PI / 180) * angle;
  var cos = Math.cos(radians);
  var sin = Math.sin(radians);
  var nx = cos * vec.x + sin * vec.y;
  var ny = cos * vec.y - sin * vec.x;
  
  return {x: nx, y: ny};
}

function ScaleMap(delta, focus) {
  var center;

  if(focus) {
    center = focus;
  }

  var map = _konvaStage.findOne('#map-layer');
  var screenToWorld = map.getTransform().copy().invert();
  var worldCenter = screenToWorld.point(center);

  //
  var oldScale = map.scale();
  var scale = Math.max(0.0125, oldScale.x + delta * oldScale.x);

  map.scale({
    x: scale,
    y: scale
  });

  // 
  scale = map.scale().x;
  screenToWorld = map.getTransform().copy().invert();

  var newCenter = screenToWorld.point(center);

  // NOTE:  I don't understand why using the transform doesn't generate the correct difference here and I need to use the new scale value for the movement.
  // .newCenter = map.getTransform().point(newCenter);
  
  map.move({
    x: (newCenter.x - worldCenter.x) * scale,
    y: (newCenter.y - worldCenter.y) * scale
  });

  syncPoiLayer(true);
  
  map.draw();
  updateHexGrids();
}

function syncPoiLayer(redraw) {
  var mapLayer = _konvaStage.findOne('#map-layer');
  var poiLayer = _konvaStage.findOne('#poi-layer');
  poiLayer.position(mapLayer.position());
  poiLayer.scale(mapLayer.scale());

  if(redraw) {
    poiLayer.draw();
  }
}

const _sqrt3 = Math.sqrt(3)

function renderHexFillImage(x, y, areaWidth, areaHeight, desiredRadius, scale, color) {
  var radiusFactor = _sqrt3 * 0.5;
  var radius = desiredRadius / radiusFactor * scale;
  var height = _sqrt3 * radius;
  var inRadius = height * 0.5;
  var halfRadius = radius * 0.5;
  var offsetY = inRadius - radius;

  var points = [];
  var forward = {x: -radius, y: 0}

  for(var i = 0; i < 6; ++i) {
    var p = rotateVec(forward, i * 60);
    p.x += radius + halfRadius;
    p.y += radius + offsetY;
    points.push(p);
  }

  //
  var imageWidth = radius * 2 + points[2].x - points[1].x;
  var imageHeight = height;

  x = x % imageWidth;
  y = y % imageHeight;
  
  return new Konva.Shape({
    x: x,
    y: y,
    sceneFunc: function(ctx, shape) {
      ctx.beginPath();

      var count = 0;
      
      for(var ty=-imageHeight; ty < areaHeight + imageHeight; ty += imageHeight) {
        for(var tx=-imageWidth; tx < areaWidth + imageWidth; tx += imageWidth) {
          // Wings
          ctx.moveTo(tx + points[0].x, ty + points[0].y);
          ctx.lineTo(tx + 0, ty + points[0].y);
          ctx.moveTo(tx + points[3].x, ty + points[3].y);
          ctx.lineTo(tx + imageWidth, ty + points[3].y);

          var start = points[0];
          ctx.moveTo(tx + start.x, ty + start.y);
          
          for(var i = 1; i < 6; ++i) {
            var p = points[i];
            ctx.lineTo(tx + p.x, ty + p.y);
          }
          ctx.lineTo(tx + start.x, ty + start.y);

          count++;
          if(count > 1500) {
            console.log('ERROR:  grid too dense');
            break;
          }
        }
      }

      ctx.lineWidth = 1;
      ctx.strokeStyle = color || 'white';
      ctx.stroke();
    }
  });
  
}

function updateHexGrids() {
  var map = _konvaStage.findOne('#map-layer');
  var hex = _konvaStage.findOne('#hex-layer');
  var hexFill = _konvaStage.findOne('#hex-fill');
 
  var scale = map.scale().x;
  var x = map.x();
  var y = map.y();

  var pixelsPerMile = _mapData.data.pixelsPerMile;

  var grids = hex.children;
  var mapDataGrids = _mapData.data.grid;
  
  var gridColor = ['rgba(255,255,255,0.8)', 'rgba(0,0,0,0.8)', 'rgba(0,255,255,0.8)'];

  // TEMP:  Should possibly not totally rebuild everything
  hexFill.destroyChildren();

  for(var i = 0; i < mapDataGrids.length; ++i) {
    var data = mapDataGrids[i];

    // TODO:  Check display size
    if(pixelsPerMile * scale < data[0] || data[0] == 0 || data[1] == 0) {
      continue;
    }

    var thisGrid = renderHexFillImage(x, y, hex.width(), hex.height(),
                                      data[1] * pixelsPerMile * 0.5,
                                      scale, gridColor[i % gridColor.length]);
    hexFill.add(thisGrid);
  }
  
  hexFill.draw();
}

function buildImageSources(data, callback) {
  // TODO:  Possibly validate the data.  Although that would be better done on the server side.

  //
  var imageReferences = [];
  
  //
  for(var k in data.fragments) {
    imageReferences.push(data.fragments[k].img);
  }
  
  // for(var k in data.icons) {
  //   var icon = data.icons[k];
  //   imageReferences.push(icon.img);
  // }

  if(data.data.icons) {
    imageReferences.push(data.data.icons.img);
  }

  // HACK:  Forcing default icons
  imageReferences.push("overland-icons.png");
  
  var loadedCount = 0;
  var totalToLoad = imageReferences.length;

  if(totalToLoad == 0) {
    callback(_imageSources);
    return;
  }
  
  imageReferences.forEach(function(imageName) {
    if(!_imageSources[imageName]) {
      var fragImage = new Image();

      var sourceEntry = {};
      sourceEntry.obj = fragImage;
      
      fragImage.onload = function() {
        sourceEntry.loaded = true;

        // TODO:  Loading bar
        loadedCount++;
        if(loadedCount == totalToLoad) {
          callback(_imageSources);
        }
      }

      fragImage.src = imageName;
      
      _imageSources[imageName] = sourceEntry;
    }
    else {
      loadedCount++;
      if(loadedCount == totalToLoad) {
        callback(_imageSources);
      }
    }
  });
}

function clearStage(stage) {
  var frags = stage.findOne('#fragment-container');
  var poi = stage.findOne('#poi-container');
  frags.destroyChildren();
  poi.destroyChildren();
}

function renderMapFragments(stage, data, mapPixelsPerMile) {
  var container = stage.findOne('#fragment-container');

  // Destroy any removed fragments
  var missing = container.getChildren(node => {
    return !node.fragName || !data.fragments[node.fragName];
  });

  for(var i = 0; i < missing.length; ++i) {
    missing[i].destroy();
  }

  var zOrder = [];
  
  for(var k in data.fragments) {
    var frag = data.fragments[k];
    var scale = (frag.pixelsPerMile || frag.pixelspermile) / mapPixelsPerMile;
    var imageSource = _imageSources[frag.img];
    var imageObj = imageSource.obj;
    var shapeId = `frag-${k}`;

    var mileX = frag.x * mapPixelsPerMile;
    var mileY = frag.y * mapPixelsPerMile;

    var existing = container.findOne(`#${shapeId}`);

    if(existing) {
      existing.x(mileX);
      existing.y(mileY);
      existing.width(imageObj.width);
      existing.height(imageObj.height);
      existing.scale({x: scale, y: scale});

      zOrder.push([existing, frag.layer || 0]);
    }
    else {
      var img = new Konva.Image({
        id: shapeId,
        x: mileX,
        y: mileY,
        width: imageObj.width,
        height: imageObj.height,
        scaleX: scale,
        scaleY: scale,
        image: imageObj
      });

      img.fragName = k;

      container.add(img);

      zOrder.push([img, frag.layer || 0]);
    }
  }

  // Z-indexing in Konva is just position in list of children, so it can't really be used as in other systems out of the box.
  zOrder.sort((a,b) => {
    return a[1] - b[1];
  });

  for(var i = 0; i < zOrder.length; ++i) {
    zOrder[i][0].moveToTop();
  }
  
  // TODO:  Batch draw?
  stage.draw();
}

function renderMapPoi(stage, data, mapPixelsPerMile) {
  var pixelsPerMile = _mapData.data.pixelsPerMile;

  var container = stage.findOne('#poi-container');
  var allLabels = [];
  
  for(var k in data.poi) {
    var poi = data.poi[k];
    var x = poi.x * mapPixelsPerMile;
    var y = poi.y * mapPixelsPerMile;

    // Only admin can see hidden poi
    if(poi.hidden && !_partyData.admin) {
      continue;
    }
    
    //
    var label = new Konva.Label({
      x: x,
      y: y,
      opacity: 0.5
    });

    label.add(new Konva.Tag({
      fill: poi.hidden ? 'red' : 'black',
      shadowColor: 'black',
      shadowBlur: 10,
      shadowOffset: 10,
      shadowOpacity: 0.5
    }));

    label.add(new Konva.Text({
      text: `${k}`,
      padding: 5,
      fontSize: 30,
      fontFamily: 'Calibri',
      fontSize: 18,
      fill: 'white'
    }));

    // Center the label
    label.move({
      x: label.width() * -0.5,
      y: -64 + label.height() * 0.5
    });
    
    label.centerX = label.x();
    label.centerY = label.y();
    
    container.add(label);
    allLabels.push(label);

    //
    
    // TODO:  Detect existing

    if(!data.data.icons) {
      continue;
    }
    
    var atlas = data.data.icons;
    
    if(!atlas) {
      continue;
    }
    
    var imageSource = _imageSources[atlas.img];
    
    // var scale = icon.scale || 1;

    if(!imageSource) {
      continue;
    }
    
    var imageObj = imageSource.obj;

    // TODO:  Handle other layouts (ie. not just 16x16)

    var tileWidth = atlas.tile_width || 64;
    var tileHeight = atlas.tile_height || 64;
    
    var iconY = Math.floor(poi.icon / 16) * tileWidth;
    var iconX = (poi.icon % 16) * tileHeight;
    
    var img = new Konva.Rect({
      x: x + (atlas.draw_offset_x || 0),
      y: y + (atlas.draw_offset_y || 0),
      width: tileWidth,
      height: tileHeight,
      // scaleX: scale,
      // scaleY: scale,
      fillPatternImage: imageObj,
      fillPatternOffset: { x: iconX, y: iconY }
    });

    container.add(img);

    label.poi = img;
  };

  for(var i = 0; i < allLabels.length; ++i) {
    allLabels[i].moveToTop();
  }

  positionLabels(stage, allLabels);
  positionLabels(stage, allLabels);

  syncPoiLayer();
  
  stage.draw();
}

function shapeOverlap(a, b, xOffset, yOffset) {
  var aLeft = a.x() + xOffset;
  var aRight = aLeft + a.width();
  var aTop = a.y() + yOffset;
  var aBottom = aTop + a.height();

  var bLeft = b.x();
  var bRight = bLeft + b.width();
  var bTop = b.y()
  var bBottom = bTop + b.height();

  // Detect overlap
  if(aRight < bLeft ||
     aLeft > bRight ||
     aBottom < bTop ||
     aTop > bBottom) {

    return 0;
  }
  
  var x1 = Math.max(aLeft, bLeft);
  var x2 = Math.min(aRight, bRight);
  var y1 = Math.max(aTop, bTop);
  var y2 = Math.min(aBottom, bBottom);

  return (x2 - x1) * (y2 - y1);
}

function positionLabels(stage, labels) {
  var container = stage.findOne('#poi-container');
  // For each label determine the best position and move it there (favoring the middle top)
  labels.forEach(function(thisLabel) {
    // Score the three positions by determining the amount of overlapping area for each object
    var poi = thisLabel.poi;

    // Calculate the 3 positions (center, left, right)
    var centerOffset = thisLabel.centerX - thisLabel.x();
    
    var scores = [{offset: centerOffset,
                   score: 0},
                  {offset: centerOffset + thisLabel.width() * -0.5,
                   score: 0},
                  {offset: centerOffset + thisLabel.width() * 0.5,
                   score: 0}];

    container.children.forEach(function(otherObj) {
      if(otherObj != thisLabel && otherObj != poi) {
        scores.forEach(function(slot) {
          slot.score += shapeOverlap(thisLabel, otherObj, slot.offset, 0);
        });
      }
    });

    var bestSlot = scores[0];
    scores.forEach(function(slot) {
      if(slot.score < bestSlot.score) {
        bestSlot = slot;
      }
    });

    thisLabel.move({x: bestSlot.offset, y: 0});
  });
}

function rerender() {
  var pixelsPerMile = _mapData.data.pixelsPerMile;
  clearStage(_konvaStage);
  renderMapFragments(_konvaStage, _mapData, pixelsPerMile);
  renderMapPoi(_konvaStage, _mapData, pixelsPerMile);
  updateHexGrids();
}

function updateDebugData(pointerPos) {
  var debugDiv =  document.getElementById('debugView');

  var debugData = '';
  if(_userData) {
    debugData += _userData.id;
  }

  if(_partyData) {
    debugData += ' :: \'' + _partyData.party.name + '\'';
  }

  if(_mapData) {
    debugData += ' @ \'' + _mapData.data.name + '\'';

    if(pointerPos) {
      var map = _konvaStage.findOne('#map-layer');
      var pixelsPerMile = _mapData.data.pixelsPerMile;
      var screenToWorld = map.getTransform().copy().invert();
      var pos = screenToWorld.point(pointerPos);
      var scale = map.scale();
      
      debugData += '<br/>Mile: ' +
        Math.floor(pos.x / pixelsPerMile) + " : " +
        Math.floor(pos.y / pixelsPerMile) +
        " (" + pixelsPerMile * scale.x + "px/mi.)";
    }

    var map = _konvaStage.findOne('#map-layer');
    debugData += ' Scale: ' + map.scale().x;
  }

  debugView.innerHTML = debugData;
}

function sheetBeautify(val) {
  switch($.type(val)) {
  case 'string':
    return val.replace('_', ' ');

  case 'array':
    return sheetBeautify(val.join(' '));

  case 'object':
    var result = '';
    for(var k in val) {
      var v = val[k];
      
      result += sheetBeautify(k);

      if($.type(v) != 'boolean') {
        result += ': ' + v;
      }

      result += ' ';
    }
    
    return result;
  }

  return val;
}

function addSheetEntry(container, label, value) {
  $(container).append(`<div class='sheet-element'><span class='sheet-element-label'>${sheetBeautify(label)}: </span><span>${sheetBeautify(value)}</span></div>`);
}

function addSheetElement(container, data, label, key) {
  if(data[key]) {
    addSheetEntry(container, label, data[key]);
  }
  
  return key;
}

function addSheetAttack(container, data, label) {
  var attackDiv = document.createElement('div');
  $(attackDiv).addClass('sheet-attack');
  
  $(attackDiv).append(`<div class='sheet-element-label'>${label}: </div>`);

  var dataDiv = document.createElement('div');
  $(dataDiv).addClass('sheet-attack-data');
  
  for(var k in data) {
    if(!data[k]) {
      continue;
    }
    
    var attrDiv = document.createElement('div');
    $(attrDiv).append(`<span class='sheet-attack-attr'>${sheetBeautify(k)}:</span>`);
    $(attrDiv).append(`<span> ${data[k]}  </span>`);
    dataDiv.append(attrDiv);
  }
  attackDiv.append(dataDiv);
  container.append(attackDiv);
}

function addSheetSection(container, label, labelClass, sectionClass, id, func) {
  var heading = document.createElement('div');
  $(heading).addClass('sheet-section-heading');
  $(heading).addClass(labelClass);
  heading.innerHTML = label;
  
  var section = document.createElement('div');
  $(section).addClass('sheet-section');
  $(section).addClass(sectionClass);
  $(section).attr('id', id);

  $(heading).click(function() {
    $(section).toggleClass('hidden');
  });
  
  func(section);

  container.append(heading);
  container.append(section);
  
  return section;
}

function createCharacterSheet(target, sheet) {
  var container = target[0];
  var sheetDiv = document.createElement('div');

  $(sheetDiv).addClass("sheet hidden");
  $(sheetDiv).attr('id', `character-sheet-${sheet.name}`);

  var closeButton = document.createElement('div');
  closeButton.innerHTML = 'X';
  $(closeButton).addClass('close-button');
  sheetDiv.append(closeButton);

  $(closeButton).click(function() {
    $(sheetDiv).toggleClass('hidden');
  });

  var handledKeys = ['name', 'str', 'dex', 'con', 'int', 'wis', 'char', 'proficiencies', 'saves', 'attacks', 'ac'];
  
  //
  addSheetSection(sheetDiv, sheet.name, 'name', '', `character-sheet-${sheet.name}-stats`,
                  function(statsDiv) {
                    handledKeys.push(addSheetElement(statsDiv, sheet, "Stats", "stats"));
                    handledKeys.push(addSheetElement(statsDiv, sheet, "Level", "level"));
                    handledKeys.push(addSheetElement(statsDiv, sheet, "Race", "race"));
                    handledKeys.push(addSheetElement(statsDiv, sheet, "Type", "archetype"));
                    handledKeys.push(addSheetElement(statsDiv, sheet, "Class", "class"));
                    handledKeys.push(addSheetElement(statsDiv, sheet, "XP", "xp"));
                    handledKeys.push(addSheetElement(statsDiv, sheet, "HP", "hp"));
                    handledKeys.push(addSheetElement(statsDiv, sheet, "Casting", "casting"));
                    handledKeys.push(addSheetElement(statsDiv, sheet, "Armor", "armor"));
                    handledKeys.push(addSheetElement(statsDiv, sheet, "Speed", "speed"));

                    addSheetEntry(statsDiv, "Saves", Object.keys(sheet.saves).join(", "));
                  });
  
  // Unknown keys (for things like fighting style or other free-form data
  var hasMisc = false;

  for(var k in sheet) {
    if(!handledKeys.includes(k)) {
      hasMisc = true;
    }
  }

  if(hasMisc) {
    addSheetSection(sheetDiv, 'Misc.', 'sheet-misc', '', `character-sheet-${sheet.name}-misc`,
                    function(miscDiv) {
                      for(var k in sheet) {
                        if(!handledKeys.includes(k)) {
                          addSheetElement(miscDiv, sheet, k, k);
                         }
                      }
                    });
  }
  
  addSheetSection(sheetDiv, "Proficiencies:", '', 'sheet-proficiencies', `sheet-${sheet.name}-proficiencies`,
                  function(proDiv) {
                    for(var k in sheet.proficiencies) {
                      var proClass = (sheet.proficiencies[k] == "exp") ? 'sheet-proficiency-expertise' : 'sheet-proficiency';
                      
                      $(proDiv).append(`<div class='${proClass}'>${k}</div>`);
                    }
                  });

  
  // Attacks
  addSheetSection(sheetDiv, "Attacks:", '', 'sheet-attacks', `sheet-${sheet.name}-attacks`,
                  function(attackDiv) {
                    for(var k in sheet.attacks) {
                      addSheetAttack(attackDiv, sheet.attacks[k], k);
                    }
                  });

  // TODO:  Free-form data from the sheet body
  
  container.append(sheetDiv);
}

function updateCharacterSheets(sheets) {
  var characterPanel = $('#character-sheets-panel');

  // Save sheet/section visibility
  var elementsHidden = {};
  characterPanel.find('.sheet').each(function() {
    elementsHidden[$(this).attr('id')] = $(this).hasClass('hidden');
  });

  characterPanel.find('.sheet-section').each(function() {
    elementsHidden[$(this).attr('id')] = $(this).hasClass('hidden');
  });

  // Build sheets
  characterPanel.empty();
  
  for(var name in sheets) {
    createCharacterSheet(characterPanel, sheets[name]);
  }

  // Restore visibility
  for(var k in elementsHidden) {
    var thisElement = characterPanel.find(`#${k}`);
    var hide = elementsHidden[k]
    
    if(thisElement) {
      if(hide) {
        thisElement.addClass('hidden');
      }
      else {
        thisElement.removeClass('hidden');
      }
    }
  }
}

function updateActiveUsers(users) {
  var previous = _partyData.active_users;
  var status = {}

  // Mark new
  for(var i = 0; i < users.length; ++i) {
    var thisUser = users[i];
    var v = status[thisUser] || 0;
    status[thisUser] = v + 1;
  }

  // Mark old
  for(var i = 0; i < previous.length; ++i) {
    var thisUser = previous[i];
    var v = status[thisUser] || 0;
    status[thisUser] = v + 2;
  }

  // At this point 1 means new, 2 means missing, and 3 means still logged in

  _partyData.active_users = users;
  users.sort((a, b) => {
    var aAdmin = _partyData.users[b];
    var bAdmin = _partyData.users[a];

    if(aAdmin != bAdmin) {
      return aAdmin - bAdmin;
    }
    
    return a.localeCompare(b);
  });
  
  //
  var layer = _konvaStage.findOne('#active-users-layer');

  // TODO:  Detect existing portraits and reuse them.
  layer.destroyChildren();

  const radius = 50;
  const stroke = 3;
  const spacing = radius * 2;
  const fontSize = 20;

  var bottom = layer.height() - 15;
  var left = _partyData.admin ? 300 : 100;

  for(let i = 0; i < users.length; ++i) {
    let thisUser = users[i];
    let thisStatus = status[thisUser];
    let avatar = _partyData.avatars[thisUser];
    let alias = _partyData.aliases[thisUser];

    let img = _imageSources[avatar];
    let cached = !!img;

    if(!cached) {
      img = new Image();
      _imageSources[avatar] = img;
    }
    
    let targetY = bottom - radius;
    let x = left + i * (radius + spacing) + radius + stroke;
    
    let onload = function() {
      var scale = radius * 2 / img.width;
      
      var portrait = new Konva.Circle({
        x: x,
        y: targetY,
        radius: radius,
        fillPatternImage: img,
        fillPatternScaleX: scale,
        fillPatternScaleY: scale,
        fillPatternOffsetX: -radius / scale,
        fillPatternOffsetY: -radius / scale,
        stroke: "rgb(255, 255, 255, 120)",
        strokeWidth: stroke,

        shadowColor: 'black',
        shadowBlur: 8,
        shadowOffset: {x : 5, y : 5},
        shadowOpacity: 0.5
      });

      layer.add(portrait);

      // Name plate
      var nameplate = new Konva.Label({
        x: x,
        y: bottom - fontSize * 0.5 - 3
      });

      nameplate.add(new Konva.Tag({
        fill: '#03030380'
      }));

      nameplate.add(new Konva.Text({
        fill: 'lightgreen',
        fontSize: fontSize,
        padding: 2,
        text: alias
      }));

      nameplate.setOffset({ x: nameplate.getWidth() / 2 });
      layer.add(nameplate);
      
      layer.draw();

      // Perform the effect
      switch(thisStatus) {
      case 1:
        portrait.y(bottom + radius);
        portrait.scale({x: 0.25, y: 0.25});
        
        portrait.to({
          duration: 0.5,
          y: targetY,
          scaleX: 1,
          scaleY: 1,
          easing: Konva.Easings.EaseInOut
        });
        break;

      case 2:
        portrait.to({
          duration: 0.4,
          y: bottom + radius,
          scaleX: 0.25,
          scaleY: 0.25,
          easing: Konva.Easings.EaseInOut,
          
          onFinish: function() {
            portrait.destroy();
          }
        });
        break;
      }
    }

    if(!cached) {
      img.onload = onload;
      img.src = avatar;
    }
    else {
      onload();
    }
  }
}

function placePendingPoi(id, x, y) {
  // Convert to map mile coordinates
  var pixelsPerMile = _mapData.data.pixelsPerMile;
  var screenToWorld = _worldToScreen.inverse();
  var world = svg.createSVGPoint();
  world.x = x;
  world.y = y;
  
  world = world.matrixTransform(screenToWorld);
  
  // Add to data
  var poi = _mapData.poi_pending[id];
  poi.x = world.x / pixelsPerMile;
  poi.y = world.y / pixelsPerMile;
  _mapData.poi[id] = poi;

  delete _mapData.poi_pending[id];

  render();
  
  // Send broadcast
  sendToService(_serverConnection,
                { type: 'delta',
                  payload: {
                    map: _mapData.data.name,
                    a: {
                      poi: {
                        [id]: poi
                      }
                    },
                    d: {
                      poi_pending: [
                        id
                      ]
                    }
                  }
                });
}

function redirectToLoadParty(party) {
  window.location = `${window.location.origin}${window.location.pathname}?party=${party}`;
}

// TODO:  Everything except the auth command should send along auth/identification data

function doAcceptInvite(conn, invite) {
  sendToService(conn, {type: 'accept-invite', payload: {invite: invite}});
}

export function doPartyLoad(conn, data) {
  console.log(data)
  sendToService(conn, {type: 'loadparty', payload: data});
}

export function doMapLoad(conn, data) {
  sendToService(conn, {type: 'loadmap', payload: data});
}

function logServiceResponse(response) {
  switch(response.type) {
  case 'broadcast':
  case 'delta':
  case 'party-heartbeat-response':
    if(!_logAll) {
      return;
    }
  }

  console.log(response);
}

export function connectToService(url) {
  console.log(`Connecting to ${url}`)
  
  var queryParams = new URLSearchParams(window.location.search);

  // if user is running mozilla then use it's built-in WebSocket
  window.WebSocket = window.WebSocket || window.MozWebSocket;

  var connection = new WebSocket(url);
  _serverConnection = connection;

  connection.onopen = function () {
    var authCode = queryParams.get('code');
    var accessToken = loadAccessToken();
    
    var data = {
      code: authCode,
      token: accessToken
    }

    sendToService(connection, {type: 'auth', payload: data});
  };

  connection.onerror = function (error) {
    // an error occurred when sending/receiving data
  };

  connection.onmessage = function (message) {
    try {
      var response = JSON.parse(message.data);
      logServiceResponse(response);

      // Dispatch
      var handler = _serviceHandlers[response.type];
      var result;
      
      if(handler) {
        result = handler(connection, response);

        // Call any registered temporary callbacks
        var cb = _serviceCallbacks[response.type];

        if(cb) {
          if(result && cb.success) {
            cb.success(response);
          }
          else if(!result && cb.failure) {
            cb.failure(response);
          }

          delete _serviceCallbacks[response.type];
        }
      }
      else {
        // TODO:  Error
      }
    }
    // catch (e) {
    //   // TODO:  Add the output of the exception content.
    //   console.log('This doesn\'t look like a valid JSON: ',
    //               e.message,
    //               "\n",
    //               message.data);
    //   return;
    // }
    finally {}
  };

  return connection;
}

export function sendToService(conn, data, success, failure) {
  if(success || failure) {
    _serviceCallbacks[data.type + '-response'] = {success: success, failure: failure}
  }
  
  conn.send(JSON.stringify(data));
}

function handleLoad(e) {
  _queryParams = new URLSearchParams(window.location.search);

  _konvaStage = new Konva.Stage({
    container: 'graphics',
    width: window.innerWidth,
    height: window.innerHeight
  });

  var poiLayer = new Konva.Layer({
    id: 'poi-layer'
  });
  
  var layer = new Konva.Layer({
    id: "map-layer",
    draggable: true,

    dragBoundFunc: function(pos, evt) {
      // Very minor feature of not moving if alt is being pressed (so pings don't accidentally pan)
      var orig = this.getAbsolutePosition();

      if(evt && evt.altKey) {
        return orig;
      }

      poiLayer.position(pos);
      poiLayer.draw();

      return pos;
    }
  });

  // Handle window resizing
  $(window).resize(function () {
    _konvaStage.width(window.innerWidth);
    _konvaStage.height(window.innerHeight);

    if(_partyData) {
      updateActiveUsers(_partyData.active_users);
    }

    if(_mapData) {
      updateHexGrids();
    }
  });

  var fragmentGroup = new Konva.Group({id: 'fragment-container'})
  layer.add(fragmentGroup);
  _konvaStage.add(layer);

  //
  var hexLayer = new Konva.FastLayer({id: "hex-layer"})
  _konvaStage.add(hexLayer);

  var hexFill = new Konva.FastLayer({id: 'hex-fill'});
  _konvaStage.add(hexFill);

  //
  var poiGroup = new Konva.Group({id: 'poi-container'})
  var pingGroup = new Konva.Group({id: 'ping-container'})
  poiLayer.add(poiGroup);
  poiLayer.add(pingGroup);
  _konvaStage.add(poiLayer);
  
  var activeUserLayer = new Konva.Layer({
    id: 'active-users-layer'
  });
  _konvaStage.add(activeUserLayer);

  //
  var websocketProtocol = 'ws';

  if(window.document.location.protocol === 'https:') {
    websocketProtocol = 'wss';
  }
  
  _serverConnection = connectToService(`${websocketProtocol}://${location.host}/service`);

  startHeartbeat();

  //
  _konvaStage.addEventListener("mousemove", handleMouseMove, false);    
  _konvaStage.addEventListener("dragmove", handleMapDrag, false);
  _konvaStage.addEventListener("wheel", handleWheel, false);
  _konvaStage.addEventListener("click", handleMapClick, false);
  
  // Special keystrokes
  var container = _konvaStage.container();
  container.tabIndex = 1;
  container.focus();

  container.addEventListener("keydown", handleKeyDown, false);
  container.addEventListener("mouseover",
                             () => {
                               container.focus();
                             }, false);

  $('#tools').menu({
    items: "> :not(.ui-widget-header)",
    select: handleMenuSelection
  });

  setupPartyCreationDialog();
  setupMapCreationDialog();
  setupPropertyPanels();
  setupFileUploadDialog();
  setupMapFragmentCreation();
}

function handleKeyDown(e) {
  if(!_mapData) {
    return;
  }

  var redrawStage = false;
  
  // Map movement
  const moveAmount = 40;
  var mapLayer = _konvaStage.findOne('#map-layer');
  var pos = {x: mapLayer.x(),
             y: mapLayer.y()}

  switch(e.keyCode) {
  case 38:
    pos.y += moveAmount;
    redrawStage = true;
    break;
  case 40:
    pos.y -= moveAmount;
    redrawStage = true;
    break;
  case 37:
    pos.x += moveAmount;
    redrawStage = true;
    break;
  case 39:
    pos.x -= moveAmount;
    redrawStage = true;
    break;
  }

  mapLayer.x(pos.x);
  mapLayer.y(pos.y);

  if(redrawStage) {
    _konvaStage.draw();
    updateHexGrids();
  }
  //
  
  ////////
  switch(e.key) {
  case "Escape":
    changeMode(Modes.VIEW);
    break;
  }
}

function handleWheel(e) {
  if(!_mapData || !e.deltaY) {
    return;
  }
  
  var map = _konvaStage.findOne('#map-layer');
  var scale = map.scale();
  var mouseZoomScale = -0.125;
  var delta = Math.max(-1, Math.min(1, e.deltaY || e.wheelDelta)) * mouseZoomScale;

  var pointer = _konvaStage.getPointerPosition();
  ScaleMap(delta, pointer);
  updateDebugData(pointer);

  return false;
}

function handleMapClick(e) {
  if(!e.altKey || !_mapData) {
    return;
  }

  var map = _konvaStage.findOne('#map-layer');
  var screenToWorld = map.getTransform().copy().invert();
  var payload = screenToWorld.point(_konvaStage.getPointerPosition());

  // TODO:  Create smaller ping effect as immediate feedback
  
  sendToService(_serverConnection,
                {
                  type: 'ping',
                  payload: payload
                });
}

function handleMouseMove(e) {
  if(!_mapData) {
    return;
  }
  
  updateDebugData(_konvaStage.getPointerPosition());
}

function handleMapDrag(e) {
  if(!_mapData) {
    return;
  }

  updateHexGrids();
}
  
function handleMouseClick(e) {
  if(!_mapData) {
    return;
  }
  
  switch(_mode) {
  case Modes.POI:
    placePendingPoi(Object.keys(_mapData.poi_pending)[0], e.clientX, e.clientY);
    break;
  }
}

//
// $(handleLoad);

//////////////////
export function sendBindingRequest(inviteId, party) {      
  sendToService(_serverConnection, {
    type: 'bindparty',
    payload: {
      invite: inviteId,
      party: party
    }
  });
}

export function createParty(name) {
  if(!name || name === "") {
    return false;
  }
  
  sendToService(_serverConnection, {
    type: 'createparty',
    payload: {
      name: name
    }
  });
}

export function createMap(data) {
  if(!data.name || data.name === "") {
    return false;
  }
  
  sendToService(_serverConnection, {
    type: 'clonemap',
    payload: {
      source_type: 'template',
      source_id: 0,
      destination_type: 'map',
      config: data
    }
  });
}

function sendFragmentDelta(name) {
  var delta = {fragments: {}}
  delta.fragments[name] = _mapData.fragments[name];
  sendMapDelta(delta);
}

export function sendMapDelta(mapId, add, del) {
  sendToService(_serverConnection, {
    type: 'delta',
    payload: {
      map: mapId,
      a: add,
      d: del
    }
  });
}

export function startHeartbeat() {
  sendToService(_serverConnection, { type: 'party-heartbeat' });
  
  setTimeout(function() {
    startHeartbeat();
  }, _heartbeat);
}

function retrieveStats() {
  sendToService(_serverConnection, {
    type: 'party-stats',
    payload: {}
  });
}
