const _adminUI = [
  '#tools-panel'
];

const _playerToolsUINames = [
  'parties',
  'sheets',
  'toggle-chat',
];

function filterPlayerInterface() {
  for(var selector of _adminUI) {
    $(selector).hide();
  }

  var tools = $('#tools');
  tools.children('li').each(function() {
    if(!_playerToolsUINames.includes($(this).attr('name'))) {
      $(this).hide();
    }
  });
}

//// Dialogs
function setupPartyCreationDialog() {
  $('#create-party').dialog({
    modal: true,
    autoOpen: false,
    draggable: false,

    open: function () {
      $('#create-party-name').val('').focus();
    },

    buttons: {
      Create: function () {
        createParty($('#create-party-name').val());
        $(this).dialog("close");
      }
    }
  });
}

function setupMapCreationDialog() {
  $('#create-map').dialog({
    modal: true,
    autoOpen: false,
    draggable: false,

    open: function () {
      $('#create-map-name').val('').focus();
    },

    buttons: {
      Create: function () {
        var data = {
          name: $(this).find('#create-map-name').val(),
          pixelsPerMile: parseFloat($(this).find('#create-map-ppm').val())
        }

        createMap(data);
        $(this).dialog("close");
      }
    }
  });
}

function setupFileUploadDialog() {
  $('#file-upload').dialog({
    modal: true,
    autoOpen: false,
    draggable: false,

    open: function() {
      sendToService(_serverConnection, { type: 'party-files' },
                    (fileResponse) => {
                      var fileMenu = $('#file-upload-target');

                      fileMenu.empty();
                      fileMenu.append('<option/>');
                      for(var k in fileResponse.payload.files) {
                        var name = fileResponse.payload.files[k];
                        fileMenu.append(`<option value="${k}">${name}</option>`);
                      }
                    });
    },
    
    buttons: {
      Upload: function() {
        var fileTarget = $('#file-upload-target')[0].value;
        var req = {type: 'request-upload', payload: {}}

        if(fileTarget != '') {
          req.payload.replace = fileTarget;
        }

        sendToService(_serverConnection,
                      req,
                      (response) => {
                        var uploadId = response.payload.upload_id;
                        var fileData = $('#file-upload #file-to-upload').prop('files')[0];
                        var data = new FormData();

                        data.append('file', fileData);

                        $.ajax({
                          url: `/upload?a=${uploadId}`,
                          headers: {
                            'Authorization': JSON.stringify(loadAccessToken())
                          },
                          type: 'post',
                          data: data,
                          dataType: 'text',
                          cache: false,
                          contentType: false,
                          processData: false,

                          success: function() {
                            alert("Upload successful");
                          },

                          failure: function() {
                            alert("Upload failed");
                          }
                        });
                      });
        
        $(this).dialog("close");
      }
    }
  });
}

function setupMapFragmentCreation() {
  $('#create-fragment').dialog({
    modal: true,
    autoOpen: false,
    draggable: false,

    open: function () {
      sendToService(_serverConnection, { type: 'party-files' },
                    (response) => {
                      var fileMenu = $('#create-fragment-file');

                      fileMenu.empty();
                      for(var k in response.payload.files) {
                        var name = response.payload.files[k];
                        fileMenu.append(`<option value="${k}">${name}</option>`);
                      }
                    });
      $('#create-fragment-ppm').val(_mapData.data.pixelsPerMile);
    },

    buttons: {
      Create: function () {
        var name = $(this).find('#create-fragment-file option:selected').text();
        var data = {
          hidden: true,
          pixelsPerMile: parseFloat($(this).find('#create-fragment-ppm').val()),
          img: $(this).find('#create-fragment-file').val(),
          x: 0,
          y: 0
        }

        console.log(data);
        _mapData.fragments[name] = data;

        // Send delta
        var delta = {fragments: {}}
        delta.fragments[name] = data;
        sendMapDelta(delta);

        //
        updateMapFragmentPanel(name, data, {name: name});
        
        buildImageSources(_mapData, () => {
          renderMapFragments(_konvaStage, _mapData, _mapData.data.pixelsPerMile);
        });

        $(this).dialog("close");
      }
    }
  });
}

function setupPropertyPanels() {
  $('#properties-panel #map-properties #map-name')[0].onchange = (ev) => {
    _mapData.data.name = ev.target.value;
    updateDebugData();
    updateMapPropertyPanel(_mapData);
    sendMapDelta({data: _mapData.data})
  }

  $('#properties-panel #map-properties #map-ppm')[0].onchange = (ev) => {
    _mapData.data.pixelsPerMile = parseFloat(ev.target.value);
    updateDebugData();
    updateMapPropertyPanel(_mapData);
    sendMapDelta({data: _mapData.data})
  }
  
  $('#properties-panel #map-properties #map-set-view')[0].onclick = () => {
    var layer = _konvaStage.findOne('#map-layer');
    var scale = layer.scale();
    var ppm = _mapData.data.pixelsPerMile;

    _mapData.data.panX = layer.x() - _konvaStage.width() * 0.5;
    _mapData.data.panY = layer.y() - _konvaStage.height() * 0.5;
    _mapData.data.scale = scale.x;
    
    sendMapDelta({data: _mapData.data})
  }
  
  $('#properties-panel #map-properties #add-map-grid')[0].onclick = () => {
    _mapData.data.grid.push([0, 0]);
    updateMapPropertyPanel(_mapData);
  }

  var fragmentLayer = $('#properties-panel #fragment-properties #map-fragment-panel #frag-layer')[0];
  fragmentLayer.onchange = function(ev) {
    if(_currentFragment) {
      var val = parseInt(ev.target.value);
      _mapData.fragments[_currentFragment].layer = val;
      sendFragmentDelta(_currentFragment);
    }
  }

  var fragmentPpm = $('#properties-panel #fragment-properties #map-fragment-panel #frag-ppm')[0];
  fragmentPpm.onchange = function(ev) {
    if(_currentFragment) {
      _mapData.fragments[_currentFragment].pixelsPerMile = parseFloat(ev.target.value);
      sendFragmentDelta(_currentFragment);
    }
  }

  $('#properties-panel #fragment-properties #add-map-fragment')[0].onclick = () => {
    $('#create-fragment').dialog('open');
  }

  $('#properties-panel #fragment-properties #delete-map-fragment')[0].onclick = function() {
    var name = _currentFragment;

    if(name) {
      delete _mapData.fragments[name];
      clearMapFragmentPanel();
      renderMapFragments(_konvaStage, _mapData, _mapData.data.pixelsPerMile);

      sendMapDelta(undefined, {fragments: [name]});
    }
  }
}

function clearMapFragmentPanel() {
  var form = clearForm('#map-fragment-panel',
                       ['frag-name', 'frag-ppm']);

  form.currentFrag = undefined;
}

function updateMapPropertyPanel(data) {
  populateForm('#properties-panel #map-properties',
               {'map-name': 'name',
                'map-ppm': 'pixelsPerMile'},
               data.data);

  var grids = data.data.grid;
  var gridContainer = $('#properties-panel #map-grids');
  gridContainer.empty();

  gridContainer.append('<legend>Hexes</legend>');

  //
  for(let i = 0; i < grids.length; ++i) {
    let thisGrid = grids[i];
    var div = $('<div>').appendTo(gridContainer);

    var ppmLabel = $('<span>',
                     {text: `${thisGrid[0]} px/mi.`
                     }).appendTo(div);
    $('<input>',
      {type: 'text',
       value: thisGrid[1],
       change: (ev) => {
         thisGrid[1] = parseInt(ev.target.value);
         updateHexGrids();
         updateMapPropertyPanel(data);
         sendMapDelta({data: _mapData.data})
       }
      }).appendTo(div);

    $('<label>',
      {for: 'miPerHex',
       text: 'mi./hex'}).appendTo(div);
    
    $('<button>',
      {text: 'set',
       click: () => {
         var map = _konvaStage.findOne('#map-layer');
         var scale = map.scale();
         var ppm = data.data.pixelsPerMile * scale.x;

         thisGrid[0] = ppm.toFixed(2);

         updateHexGrids();
         updateMapPropertyPanel(data);
         sendMapDelta({data: _mapData.data})
       }
      }).appendTo(div);

    $('<button>',
      {text: '-',
       click: () => {
         grids.splice(i, 1);
         updateHexGrids();
         updateMapPropertyPanel(data);
         sendMapDelta({data: _mapData.data})
       }
      }).appendTo(div);
  }
}

function updateMapFragmentPanel(name, fragData) {
  var form = populateForm('#map-fragment-panel',
                          {'frag-name': 'name',
                           'frag-ppm': 'pixelsPerMile',
                           'frag-x': 'x',
                           'frag-y': 'y',
                           'frag-layer': 'layer'},
                          fragData,
                          {name: name});

  form.currentFrag = name;
}

////////
function clearForm(target, fields) {
  var form = $(target);

  for(var fieldId in fields) {
    var field = form.find(`#${fieldId}`);

    if(field) {
      var key = fields[fieldId];

      field.val('');
      field.text('');
    }
  }

  return form;
}

function populateForm(target, mapping, data, aux) {
  var form = $(target);

  for(var fieldId in mapping) {
    var field = form.find(`#${fieldId}`);

    if(field) {
      var key = mapping[fieldId];
      var val = data[key];

      if(!val) {
        val = aux[key];
      }

      if(!val) {
        val = '';
      }
      
      field.val(val);
      field.text(val);
    }
  }

  return form;
}

function getMenuPath(item) {
  var path = [];

  while(item && item != document.body && item.id != 'tools') {
    path.push($(item).attr('name'));
    item = item.parentNode;
  }

  return path.reverse().join('/');
}

function handleMenuSelection(event, ui) {
  var item = ui.item[0];
  var menuPath = getMenuPath(item);
  var handler = _menuHandlers[menuPath];

  if(handler) {
    return handler(item);
  }
  
  console.log(`Unhandled menu path '${menuPath}'`);
}

//////////////////  Menu handlers
registerMenuHandler('toggle-chat', () => {
  var panel = $("#chat-panel");
  panel.toggle();
});

registerMenuHandler('set-mode', (item) => {
  var modeName = $(item).attr('mode');

  changeMode(Modes[modeName]);
});

////
function updatePartyMenu(data) {
  var tools = $('#tools');
  var list = tools.find('#party-list');

  list.empty();
  
  for(var i = 0; i < data.length; ++i) {
    var party = data[i];
    list.append(`<li name="party" party="${party.id}"><div>${party.name}</div></li>`);
  }

  tools.menu('refresh');
}

registerMenuHandler('create-party', () => {
  $("#create-party").dialog('open');
});

registerMenuHandler('parties/list/party', (item) => {
  var party = $(item).attr('party');
  console.log(`LOAD: ${party}`);

  redirectToLoadParty(party);
});

////
function updateMapMenu(data) {
  var tools = $('#tools');
  var list = tools.find('#map-list');

  list.empty();
  
  for(var i = 0; i < data.length; ++i) {
    var map = data[i];
    list.append(`<li name="map" mapid="${map.id}"><div>${map.name}</div></li>`);
  }

  tools.menu('refresh');
}

registerMenuHandler('create-map', () => {
  $("#create-map").dialog('open');
});

registerMenuHandler('maps/list/map', (item) => {
  var map = $(item).attr('mapid');

  if(_mapData && map == _mapData.id) {
    return false;
  }
  
  console.log(`LOAD: ${map}`);
  doMapLoad(_serverConnection, {map: map});
});

////
function updateCharacterSheetMenu(data) {
  var tools = $('#tools');
  var list = tools.find('#sheet-list');

  list.empty();

  console.log(data);
  
  for(var k in data) {
    var sheet = data[k];
    list.append(`<li name="sheet" sheetid="${sheet.name}"><div>${sheet.name}</div></li>`);
  }

  tools.menu('refresh');
}

registerMenuHandler('sheets/list/sheet', (item) => {
  var sheet = $(item).attr('sheetid');
  var sheetDiv = $(`#character-sheet-${sheet}`);

  if(sheetDiv) {
    sheetDiv.toggleClass('hidden');
  }
});

//
registerMenuHandler('upload-file', () => {
  $('#file-upload').dialog('open');
});

registerMenuHandler('move-party', () => {
  // Force the party to load this map.
  sendToService(_serverConnection, {
    type: 'party-switchmap',
    payload: {
      map: _mapData.id
    }
  });
});
