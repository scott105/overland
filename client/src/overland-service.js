//////////////////  Service handlers
registerServiceHandler('auth-response-OLD', function(conn, response) {
  var invite = _queryParams.get('invite');
  if(!invite) {
    invite = Cookies.get('invite');
  }
  
  switch(response.status) {
  default:
    // TODO:  Log
    return false;
    
  case 403:
    clearAccessToken();
    
    //redirect
    if(invite) {
      Cookies.set('invite', invite, { expires: 1 });
    }
    
    window.location = response.payload.redirect;
    return false;

  case 200:
    // fall through
  }

  _userData = response.payload;
  saveAccessToken(response.payload.token);

  updateDebugData();
  updatePartyMenu(response.payload.parties);

  // Handle other special request types (invites, bindings, etc.)
  if(_queryParams.has('bindchannel')) {
    showBindingUi(_queryParams.get('bindchannel'));
  }

  if(invite) {
    Cookies.remove('invite');
    doAcceptInvite(conn, invite);
  }
  else {
    // Attempt to load any query string specified party or store info on last loaded. 
    var partyToLoad = _queryParams.get('party');

    if(!partyToLoad) {
      partyToLoad = response.payload.last_party;
    }
    
    if(partyToLoad) {
      doPartyLoad(conn, {party: partyToLoad});
    }
  }
});

registerServiceHandler('loadparty-response-OLD', function(conn, response) {
  if(response.status != 200) {
    // TODO:  Error
    alert("Party load failed");
    return false;
  }

  _partyData = response.payload;

  //
  if(_partyData.chat) {
    var chatPanel = $('#chat-panel');
    
    for(var i = _partyData.chat.length - 1; i >= 0; --i) {
      var entry = _partyData.chat[i];
      addChatEntry(chatPanel, entry.tag, entry.msg);
    }
  }
  
  updateDebugData();
  updateMapMenu(response.payload.maps);
  updateActiveUsers(response.payload.active_users);
  updateCharacterSheets(response.payload.sheets);
  updateCharacterSheetMenu(response.payload.sheets);
  
  if(!_partyData.admin) {
    filterPlayerInterface();
  }
     
  if(_partyData.party.current_map) {
    doMapLoad(conn, {map: _partyData.party.current_map});
    return true;
  }
  
  return true;
});

registerServiceHandler('loadmap-response', function(conn, response) {
  if(response.status != 200) {
    // TODO:  Error
    return false;
  }

  _mapData = response.payload;

  if(!_mapData.data.icons) {
    _mapData.data.icons = {
      img: "overland-icons.png",
      tile_width: 64,
      tile_height: 64,
      draw_offset_x: -32,
      draw_offset_y: -32
    }
  }
  
  updateDebugData();
  
  var pixelsPerMile = _mapData.data.pixelsPerMile

  var map = _konvaStage.findOne('#map-layer');
  var scale = parseFloat(_mapData.data.scale);
  map.x(parseFloat(_mapData.data.panX) + _konvaStage.width() * 0.5);
  map.y(parseFloat(_mapData.data.panY) + _konvaStage.height() * 0.5);
  map.scale({x: scale, y: scale});

  //
  buildImageSources(_mapData, () => {
    clearStage(_konvaStage);
    renderMapFragments(_konvaStage, _mapData, pixelsPerMile);
    renderMapPoi(_konvaStage, _mapData, pixelsPerMile);
    updateHexGrids();
  });

  updateMapPropertyPanel(response.payload);
});

registerServiceHandler('party-switchmap', (conn, response) => {
  // If we're an admin, just set the visual map indicator instead of doing a load
  if(_partyData.admin) {
    console.log(`TODO:  Set active map indicator to ${response.payload.map}`);
  }
  else {
    doMapLoad(_serverConnection, {map: response.payload.map});
  }
});

registerServiceHandler('party-switchmap-response', (conn, response) => {
  console.log(response);
  
  if(response.status != 200) {
    alert("Failed to initiate party map load: " + response.status);
  }
});

registerServiceHandler('party-files-response', (conn, response) => {
  console.log(response);
  return true;
});

registerServiceHandler('broadcast', function(conn, response) {
  var data = response.payload;
  data.status = 200;

  if(_logAll) {
    console.log(data);
  }
  
  // Dispatch
  var handler = _serviceHandlers[data.type];
  if(handler) {
    handler(conn, data);
  }
  else {
    // TODO:  Error
  }
});

registerServiceHandler('delta', (conn, response) => {
  if(response.status != 200) {
    // TODO:  Error
    return false;
  }

  // Modify necessary data
  var additions = response.payload.a;
  for(var group in additions) {
    for(var k in additions[group]) {
      // TODO:  make this able to handle hierarchical data so that individual elements of objects can be changed
      _mapData[group][k] = additions[group][k];
    }
  }

  var deletions = response.payload.d;
  for(var group in deletions) {
    var keys = deletions[group];
    for(var i = 0; i < keys.length; ++i) {
      delete _mapData[group][keys[i]];
    }
  }

  updateDebugData();
  updateMapPropertyPanel(_mapData);
  renderMapFragments(_konvaStage, _mapData, _mapData.data.pixelsPerMile);
  updateHexGrids();
});

registerServiceHandler('update-sheets', (conn, response) => {
  for(var k in response.payload) {
    var sheet = response.payload[k];

    if(!sheet) {
      delete _partyData.sheets[k];
    }
    else {
      _partyData.sheets[k] = sheet;
    }
  }

  console.log(response.payload);
  
  updateCharacterSheets(_partyData.sheets);
  updateCharacterSheetMenu(_partyData.sheets);
});

function addChatEntry(panel, user, message) {
  var container = panel[0];
  var message = _markdown.makeHtml(message);
  var autoScroll = container.scrollTop == container.scrollTopMax;

  var userEntry = "";
  
  if(user !== _lastChatUser) {
    panel.append(`<hr/><div class='user'>${user}:</div>`);
  }
  _lastChatUser = user;
  
  panel.append(`<div class='message'> ${message}</div>`);

  if(autoScroll) {
    container.scrollTop = container.scrollTopMax;
  }
}

registerServiceHandler('chat-OLD', (conn, response) => {
  var panel = $('#chat-panel');
  var user = response.payload.tag;
  addChatEntry(panel, user, response.payload.msg);
});

registerServiceHandler('bindparty-response', (conn, response) => {
  if(response.status == 200) {
    var party = response.payload.party;
    redirectToLoadParty(party);
  }
  else {
    alert("Binding failed");
  }
});

registerServiceHandler('accept-invite-response', (conn, response) => {
  if(response.status == 200) {
    var party = response.payload.party;
    alert(`Joined ${party}`);
    redirectToLoadParty(party);
  }
  else {
    alert('Failed to join party');
  }
});

registerServiceHandler('createparty-response', (conn, response) => {
  if(response.status != 200) {
    alert("Party creation failed");
    
    // TODO:  Error
    return false;
  }

  var partyId = response.payload.id;
  
  if(_queryParams.has('bindchannel')) {
    var bindingInvite = _queryParams.get('bindchannel');
    sendBindingRequest(bindingInvite, partyId);
    return true;
  }
  
  redirectToLoadParty(partyId);
});

registerServiceHandler('clonemap-response', (conn, response) => {
  if(response.status != 200) {
    // TODO:  Error
    return false;
  }
  
  _partyData.maps = response.payload.maps;
  updateMapMenu(response.payload.maps);
});

registerServiceHandler('request-upload-response', (conn, response) => {
  return response.status == 200;
});

registerServiceHandler('ping', (conn, response) => {
  if(response.status != 200) {
    return false;
  }

  if(_logAll) {
    console.log(response.payload);
  }

  // TODO:  Create a temp shape for the ping effect
  var pos = response.payload;
  var map = _konvaStage.findOne('#map-layer');
  var container = _konvaStage.findOne('#ping-container');

  var scale = map.scale();
  
  var shape = new Konva.Ring({
    x: pos.x,
    y: pos.y,
    innerRadius: 1 / scale.x,
    outerRadius: 10 / scale.x,
    fill: '#ffff00e9'
  });

  container.add(shape);
  _konvaStage.draw();

  shape.to({
    innerRadius: 38 / scale.x,
    outerRadius: 40 / scale.x,
    duration: 0.4,
    easing: Konva.Easings.EaseInOut,
    onFinish: function() {
      shape.destroy();
    }
  });
  
});

registerServiceHandler('party-heartbeat-response', (conn, response) => {
  if(response.status != 200) {
    // TODO:  Show a connection issue icon.
    return false;
  }

  updateActiveUsers(response.payload.active_users);
});

registerServiceHandler('party-user-announce', (conn, response) => {
  if(response.status != 200) {
    return false;
  }

  var user = response.payload.user;
  var avatar = response.payload.avatar;
  var alias = response.payload.alias;
  var newActive = _partyData.active_users.slice();
  
  if(!_partyData.users[user]) {
    // TODO:  Figure out the correct thing to do here in terms of permissions
    _partyData.users[user] = false;
  }

  if(newActive.indexOf(user) == -1) {
    newActive.push(user);
  }
  
  _partyData.avatars[user] = avatar;
  _partyData.aliases[user] = alias;

  updateActiveUsers(newActive);
});

registerServiceHandler('party-stats-response', (conn, response) => {
  if(response.status != 200) {
    return false;
  }
});
