import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

export default function SimpleMenu(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        {props.heading}
      </Button>
      <Menu
        id={props.id}
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {React.Children.map(props.children, (item) => {
          var itemCallback = item.props.onClick || props.onSelection

          var cb = function() {
            itemCallback(item.props.id)
            handleClose()
          }

          return React.cloneElement(item, {onClick: cb})
        })}
      </Menu>
    </div>
  );
}
