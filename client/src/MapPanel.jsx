import React from 'react';
import SimpleMenu from './SimpleMenu';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import GenericDisplay from './GenericObjectDisplay';

import * as Overland from './overland.js';

import { Component } from 'react';

export default class MapPanel extends Component {
  constructor(props) {
    super(props);
  }

  moveParty = () => {
    // Force the party to load this map.
    Overland.sendToService(global._serverConnection, {
      type: 'party-switchmap',
      payload: {
        map: this.props.mapData.id
      }
    });
  }

  render() {
    var mapData = this.props.mapData;

    if(!mapData || !mapData.id) {
      return null;
    }

    if(!this.props.admin) {
      return null;
    }

    var appControl = this.props.appControl;

    return (
      <MenuList>
        <div>MAP STUFF</div>
        <MenuItem
          key='MoveHere'
          onClick={this.moveParty}>Move Party Here</MenuItem>
        <MenuList>
          <div>MODE</div>
          <MenuItem key='mode-view' onClick={() => { appControl.setEditMode(null); }}>View</MenuItem>
          <MenuItem key='mode-poi' onClick={() => { appControl.setEditMode('poi'); }}>POI</MenuItem>
        </MenuList>
        {/* <GenericDisplay title='Fragments' data={mapData.fragments} /> */}
        {/* <GenericDisplay title='PoI' data={mapData.poi} /> */}
      </MenuList>
    );
  }
}
