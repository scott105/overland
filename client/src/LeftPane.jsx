import React from 'react';
import { Component, useState, useEffect, useRef } from 'react';

import UserMenu from './UserMenu';
import PartyMenu from './PartyMenu';
import MapPanel from './MapPanel';

import Drawer from '@material-ui/core/Drawer';

import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

export default function LeftPane(props) {
  if(!props.open) {
    return (
      <IconButton
        aria-label="menu"
        className="LeftDrawerButton"
        onClick={props.appControl.toggleLeftPane} >
        <MenuIcon />
      </IconButton>);
  }
  
  return (
    <Drawer
      variant="persistent"
      anchor="left"
      open={props.open}
      className="LeftDrawer"
      classes={{paper: "LeftDrawerPaper"}}>

      <IconButton
        aria-label="menu"
        onClick={props.appControl.toggleLeftPane} >
        <MenuIcon />
      </IconButton>

      <UserMenu admin={props.admin} userData={props.userData} appControl={props.appControl} />
      <PartyMenu admin={props.admin} partyData={props.partyData} appControl={props.appControl} />
      <MapPanel admin={props.admin} mapData={props.mapData} appControl={props.appControl} />
    </Drawer>);}
