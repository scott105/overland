import React from 'react';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

import * as Overland from './overland.js';

import { Component } from 'react';

export default class Auth extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      authStatus: null
    };
  }

  componentDidMount() {
    var queryParams = new URLSearchParams(window.location.search);

    Overland.registerServiceHandler('auth-response', (conn, response) => {
      var authStatus = response.status;
      var userData = response.payload;

      this.setState({ authStatus: authStatus });

      if(userData.token) {
        cookies.set('token', JSON.stringify(userData.token));
      }

      // Trigger the login or whatever else.
      var invite = queryParams.get('invite');
      if(!invite) {
        invite = cookies.get('invite');
      }
      
      switch(authStatus) {
      default:
        // TODO:  Log
        return false;

      case 403:
        cookies.remove('token');

        //TODO: Invite redirect
        // if(invite) {
        //   cookies.set('invite', invite, { expires: 1 });
        // }

        window.location = userData.redirect;
        this.setState({ redirect: response.payload.redirect });

        return false;

      case 200:
        this.props.onSuccess({
          connection: this.state.connection,
          userData: response.payload
        });

        if(invite) {
          cookies.remove('invite');
          Overland.sendToService(conn, {type: 'accept-invite', payload: {invite: invite}});
        }

        Overland.startHeartbeat();

        break;
      }
      
    });

    // HACK:  Global
    global._queryParams = queryParams;

    var websocketProtocol = 'ws';

    if(window.document.location.protocol === 'https:') {
      websocketProtocol = 'wss';
    }

    var conn = Overland.connectToService(`${websocketProtocol}://${location.hostname}:${location.port}/service`);
    this.setState({ connection: conn });
  }

  componentWillUnmount() {
  }
  
  render() {
    var status = this.state.authStatus;
    if(status == 403) {
      return (
        <div>
          Auth: {status} redirect to <a href={this.state.redirect}>Discord Auth</a>
        </div>
      );
    }
    else {
      return null;
    }
  }
}
