const standardjs = require('@neutrinojs/standardjs');
const react = require('@neutrinojs/react');
// const jest = require('@neutrinojs/jest');

module.exports = {
  options: {
    root: __dirname,
  },
  use: [standardjs(),
        react({
          html: {
            title: '- O V E R L A N D -'
          }
          ,
          devServer: {
            open: false,
            host: "0.0.0.0",
            public: 'localhost:9000',
            port: 9000,
            disableHostCheck: true,
            contentBase: ['src']
          }
        })
    // ,
    //     jest()
       ]};
